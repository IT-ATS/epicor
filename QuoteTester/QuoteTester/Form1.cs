﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ice.Core;
using Erp.Proxy.BO;
//using Ice.Proxy.BO;
using Ice.Lib.Framework;
using Erp.BO;
using Erp.Adapters;
//using Ice.BO;

namespace QuoteTester
{
    public partial class Form1 : Form
    {

        Session epiSession;

        public Form1()
        {
            InitializeComponent();

            string configFile = @"C:\Epicor\ERP10.1ClientTest\Client\config\EpicorTest10.sysconfig";
            //string configFile = @"C:\Epicor\ERP10.1Client\Client\config\EpicorLive10.sysconfig";
            epiSession = new Session("manager", "manager", Session.LicenseType.Default, configFile);
            //            UserCodesImpl userCodesBO = WCFServiceSupport.CreateImpl<Ice.Proxy.BO.UserCodesImpl>(epiSession, Ice.Proxy.BO.UserCodesImpl.UriPath);

        }

        private void button1_Click(object sender, EventArgs e)
        {

            QuoteImpl quoteBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.QuoteImpl>(epiSession, Erp.Proxy.BO.QuoteImpl.UriPath);

            var quoteDs = quoteBO.GetByID(Convert.ToInt32(quoteTb.Text));

            QuoteDataSet.QuoteHedRow quoteHedRow = quoteDs.QuoteHed[0];
            QuoteDataSet.QuoteDtlDataTable quoteDtl = quoteDs.QuoteDtl;
            QuoteDataSet.QuoteHedMscDataTable quoteHedMsc = quoteDs.QuoteHedMsc;

            ATQuote aTQuote = new ATQuote(epiSession);
            aTQuote.CalculateFreight(quoteHedRow, quoteDtl, quoteHedMsc);
            //quoteHedRow.Quoted


        }

        private void button2_Click(object sender, EventArgs e)
        {
            var launcher = new ILauncher(epiSession);

            QuoteAdapter quoteAdapter = new QuoteAdapter(launcher);
            quoteAdapter.BOConnect();
            quoteAdapter.GetByID(Convert.ToInt32(quoteTb.Text));

    //        ATQuote.CalculateFreight(quoteAdapter.QuoteData.QuoteHed[0], quoteAdapter, epiSession);



            //            quoteAdapter.QuoteData.

        }
    }
}
