﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ice.Core;
using Erp.Proxy.BO;
using Ice.Lib.Framework;
using Erp.BO;
using System.Windows.Forms;
using Ice.Lib.WSProvider;
using System.Data;


namespace QuoteTester
{



    #region ATQuote


    /// <summary>
    /// History:
    /// 11/21/17 Updated freight calc CANADA to CA
    /// 11/2/17: converted CalculateFreight from VB
    /// A helper for quote entry
    /// </summary>
    class ATQuote : IDisposable
    {

        string CalculateFreightWSDL = "";
        string ShipFromState = "";
        string ShipFromZipCode = "";
        string MiscChargeCodeForFreight = "";
        string MiscChargeCodeForFreightDescription = "";
        Session epiSession;

        public ATQuote(Session _epiSession)
        {
            epiSession = _epiSession;
            using (CompanyImpl companyBO = WCFServiceSupport.CreateImpl<CompanyImpl>(epiSession, CompanyImpl.UriPath))
            {

                var companyDS = companyBO.GetByID(epiSession.CompanyID);
                CalculateFreightWSDL = (string)companyDS.Company[0]["CalculateFreightWSDL_c"];
                MiscChargeCodeForFreight = (string)companyDS.Company[0]["MiscChargeCodeForFreight_c"];
                ShipFromState = companyDS.Company[0].State;
                ShipFromZipCode = companyDS.Company[0].Zip;
            }

            using (MiscChrgImpl miscChrgBO = WCFServiceSupport.CreateImpl<MiscChrgImpl>(epiSession, MiscChrgImpl.UriPath))
            {
                var miscChargeDS = miscChrgBO.GetByID(MiscChargeCodeForFreight);
                MiscChargeCodeForFreightDescription = miscChargeDS.MiscChrg[0].Description;
            }

        }

        public void Dispose()
        {
            epiSession = null;
        }

        // AdvancedWare Freight Calculation Begin
        public void CalculateFreight(Erp.BO.QuoteDataSet.QuoteHedRow quoteHedRow, Erp.BO.QuoteDataSet.QuoteDtlDataTable quoteDtl, Erp.BO.QuoteDataSet.QuoteHedMscDataTable quoteMsc)
        {

            object freightCalcInfo;
            string shipViaCode = null;
            PartData partInfo = null;
            string errorMessage = null;
            string[] shipToAddress = new string[5];

            if (quoteHedRow != null)
            {
                shipViaCode = quoteHedRow.ShipViaCode;

                shipToAddress = GetShipToAddress(quoteHedRow);
                partInfo = GetPartInfo(quoteDtl);

                if (partInfo.TotalActualWeight > 0)
                {
                    // plp 07-19-2017 --->
                    //Dim params(11) as Object
                    object[] @params = new object[15];
                    // plp 07-19-2017 <---
                    @params[0] = shipViaCode;
                    // ShipVia Code
                    @params[1] = ShipFromState;
                    // "Ship From" State
                    @params[2] = ShipFromZipCode;
                    // "Ship From" Zip Code
                    @params[3] = shipToAddress[3];
                    // Address1
                    @params[4] = shipToAddress[0];
                    // City
                    @params[5] = shipToAddress[1];
                    // State
                    @params[6] = shipToAddress[2];
                    // Zip Code
                    @params[7] = shipToAddress[4];
                    // Country
                    @params[8] = partInfo.TotalVolumePrePackaged;
                    // TotalVolumePrePackaged
                    @params[9] = partInfo.TotalQuantityPrePackagedContainers;
                    // TotalQuantityPrePackagedContainers
                    @params[10] = partInfo.TotalVolumeNonPrePackaged;
                    // TotalVolumeNonPrePackaged
                    @params[11] = partInfo.TotalActualWeight;
                    // TotalActualWeight
                    // plp  07-19-2017 --->
                    @params[12] = partInfo.MotorMovementsRelaysProductsOnly;
                    // MotorsMovementsRelaysProductsOnly
                    @params[13] = partInfo.AnyOversizeProducts;
                    // AnyOversizeProducts
                    @params[14] = quoteHedRow.TotalGrossValue;
                    // plp  07-19-2017 <---

                    WSProvider webserviceProvider = new WSProvider();

                    try
                    {
                        //freightCalcInfo = (FreightCalcInfo) webserviceProvider.DynamicWSCall(CalculateFreightWSDL, "GetUPSRates", @params);

                        freightCalcInfo = webserviceProvider.DynamicWSCall(CalculateFreightWSDL, "GetUPSRates", @params);
                        var a = freightCalcInfo.GetType();
                        var b = a.GetField("ErrorMessage");
                        var errorMessageInfo = freightCalcInfo.GetType().GetField("ErrorMessage");
                        errorMessage = (String)(errorMessageInfo.GetValue(freightCalcInfo));

                        // errorMessage = freightCalcInfo.ErrorMessage;
                        if (errorMessage.Length == 0)
                        {
                            var DataTableInfo = freightCalcInfo.GetType().GetField("DataTable");
                            DataTable dt = (DataTable)(DataTableInfo.GetValue(freightCalcInfo));

                            DataRow row = dt.Rows[0];

                            //                            DataRow row = freightCalcInfo.DataTable.Rows[0];
                            decimal rate = (decimal)row["Rate"];
                            DialogResult createMiscCharge = MessageBox.Show("The shipping cost is: " + rate.ToString("C") + Environment.NewLine + Environment.NewLine + "Would you like to create a Miscellanous Charge for this amount?", "Freight Calculation", MessageBoxButtons.YesNo);

                            if (createMiscCharge == DialogResult.Yes)
                            {
                                CreateMiscellaneousCharge(rate, quoteHedRow, quoteMsc);
                            }
                        }
                        else
                        {
                            MessageBox.Show(errorMessage, "Freight Calculation", MessageBoxButtons.OK);
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("[CalculateFreight] Error - " + ex.Message, "Freight Calculation", MessageBoxButtons.OK);
                    }
                    finally
                    {
                        webserviceProvider = null;
                    }
                }
                else
                {
                    MessageBox.Show("Freight cannot be calculated because the total weight of the order is 0 (zero).", "Freight Calculation", MessageBoxButtons.OK);
                }
            }

        }

        private void CreateMiscellaneousCharge(decimal amount,  Erp.BO.QuoteDataSet.QuoteHedRow quoteHedRow, Erp.BO.QuoteDataSet.QuoteHedMscDataTable quoteMsc)
        {
            bool freightChargeExists = false;
            decimal freightAmount = default(decimal);
            decimal docFreightAmount = default(decimal);

            freightAmount = amount;
            docFreightAmount = freightAmount;

            freightChargeExists = false;
            QuoteImpl quoteBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.QuoteImpl>(epiSession, Erp.Proxy.BO.QuoteImpl.UriPath);

            var quoteDs = quoteBO.GetByID(quoteHedRow.QuoteNum);

            foreach (Erp.BO.QuoteDataSet.QuoteHedMscRow quoteMscRow in quoteDs.QuoteHedMsc.Rows)
            {
                if ((bool)quoteMscRow["Checkbox01"] == true)
                {
                    quoteMscRow.MiscAmt = freightAmount;
                    quoteMscRow.DocMiscAmt = docFreightAmount;
                    quoteMscRow.RowMod = "U";
                    freightChargeExists = true;
                    quoteBO.Update(quoteDs);
                    //    break; // TODO: might not be correct. Was : Exit For
                }
            }

            if (!freightChargeExists)
            {

                quoteBO.GetNewQuoteHedMsc(quoteDs, quoteHedRow.QuoteNum, 0, 0);
                var newRow = (Erp.BO.QuoteDataSet.QuoteHedMscRow)(from row in quoteDs.QuoteHedMsc.AsEnumerable() where (string)row["RowMod"] == "A" select row).FirstOrDefault();
                newRow.MiscCode = MiscChargeCodeForFreight;
                newRow.MiscAmt = freightAmount;
                newRow.DocMiscAmt = docFreightAmount;
                newRow["Checkbox01"] = true;
                newRow.FreqCode = "F";
                newRow.Description = MiscChargeCodeForFreightDescription;

                newRow.RowMod = "A";
                quoteBO.Update(quoteDs);
            }
            quoteBO.Dispose();
        }

        private string[] GetShipToAddress(Erp.BO.QuoteDataSet.QuoteHedRow quoteHedRow)
        {
            int custNum = 0;
            int countryNum = 0;
            string shipToNum = null;
            string[] shipAddressArray = new string[5];
            string shipAddress1 = null;
            string shipCity = null;
            string shipState = null;
            string shipZip = null;
            string shipCountry = null;

            bool OneTimeShip = false;





            custNum = quoteHedRow.CustNum;
            shipToNum = quoteHedRow.ShipToNum;
            OneTimeShip = quoteHedRow.UseOTS;

            if (OneTimeShip)
            {
                shipCity = quoteHedRow.OTSCity;
                shipState = quoteHedRow.OTSState;
                shipZip = quoteHedRow.OTSZIP;
                shipAddress1 = quoteHedRow.OTSAddress1;
                countryNum = quoteHedRow.OTSCountryNum;
                switch (countryNum)
                {
                    case 77:
                        shipCountry = "US";
                        break; // TODO: might not be correct. Was : Exit Select

                    case 13:
                        shipCountry = "CA";
                        break; // TODO: might not be correct. Was : Exit Select

                    default:
                        shipCountry = "";
                        // if it's not US or CANADA, we don't care what it is
                        break; // TODO: might not be correct. Was : Exit Select

                }
            }
            else
            {
                try
                {
                    ShipToImpl shipToBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.ShipToImpl>(epiSession, Erp.Proxy.BO.ShipToImpl.UriPath);

                    var shipToDS = shipToBO.GetByID(custNum, shipToNum);
                    shipToBO.Dispose();

                    shipCity = shipToDS.ShipTo[0].City;
                    shipState = shipToDS.ShipTo[0].State;
                    shipZip = shipToDS.ShipTo[0].ZIP;
                    shipAddress1 = shipToDS.ShipTo[0].Address1;
                    shipCountry = shipToDS.ShipTo[0].Country;
                }
                catch (System.Exception ex)
                {
                    shipCity = "";
                    shipState = "";
                    shipZip = "";
                    shipAddress1 = "";
                    shipCountry = "";
                }
                finally
                {
                }
            }

            if (shipCountry == "USA")
                shipCountry = "US";
            // UPS wants  US, not USA

            if (shipCountry.ToUpper() == "CANADA")
                shipCountry = "CA";


            shipAddressArray[0] = shipCity;
            shipAddressArray[1] = shipState;
            shipAddressArray[2] = shipZip;
            shipAddressArray[3] = shipAddress1;
            shipAddressArray[4] = shipCountry;

            return shipAddressArray;

        }

        private PartData GetPartInfo(Erp.BO.QuoteDataSet.QuoteDtlDataTable quoteDtl)
        {
            bool prePackaged = false;
            decimal prePackagedBoxesPerSleeve = default(decimal);
            decimal netWeight = default(decimal);
            decimal totalActualWeight = default(decimal);
            decimal orderQuantity = default(decimal);
            decimal totalVolumePrePackaged = default(decimal);
            decimal totalVolumeNonPrePackaged = default(decimal);
            decimal partLength = default(decimal);
            decimal partWidth = default(decimal);
            decimal partHeight = default(decimal);
            decimal partVolume = default(decimal);
            int TotalQuantityPrePackagedContainers = 0;
            string partNumber = null;

            bool motorMovementsRelaysProductsOnly = false;
            bool anyOversizeProducts = false;
            PartData partData = new PartData();

            totalActualWeight = 0;
            totalVolumePrePackaged = 0;
            totalVolumeNonPrePackaged = 0;
            TotalQuantityPrePackagedContainers = 0;
            motorMovementsRelaysProductsOnly = true;
            anyOversizeProducts = false;

            PartImpl partBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.PartImpl>(epiSession, Erp.Proxy.BO.PartImpl.UriPath);

            foreach (Erp.BO.QuoteDataSet.QuoteDtlRow quoteDtlRow in quoteDtl.Rows)
            {
                partNumber = quoteDtlRow.PartNum;
                orderQuantity = quoteDtlRow.OrderQty;

                try
                {
                    var partDs = partBO.GetByID(partNumber);
                    var partRow = partDs.Part[0];
                    // plp 07-19-2017 <---
                    prePackaged = (bool)partRow["CheckBox02"];
                    prePackagedBoxesPerSleeve = (decimal)partRow["Number01"];
                    netWeight = partRow.NetWeight;
                    partLength = partRow.PartLength;
                    partWidth = partRow.PartWidth;
                    partHeight = partRow.PartHeight;
                    partVolume = partLength * partWidth * partHeight;

                    // plp 07-19-2017 --->
                    //totalActualWeight += netWeight
                    totalActualWeight += netWeight * orderQuantity;
                    // plp 07-19-2017 <---

                    if (prePackaged)
                    {
                        totalVolumePrePackaged += partVolume * orderQuantity;
                        if (prePackagedBoxesPerSleeve == 0)
                            prePackagedBoxesPerSleeve = 1;
                        TotalQuantityPrePackagedContainers += Convert.ToInt32(Math.Ceiling(orderQuantity / prePackagedBoxesPerSleeve));
                    }
                    else
                    {
                        totalVolumeNonPrePackaged += partVolume * orderQuantity;
                    }
                    // plp 07-19-2017 --->
                    if ((bool)partRow["CheckBox06"] == false)
                    {
                        motorMovementsRelaysProductsOnly = false;
                    }
                    if ((bool)partRow["CheckBox07"] == true)
                    {
                        anyOversizeProducts = true;
                    }
                    // plp 07-19-2017 <---
                }
                catch (Exception e)
                {
                    MessageBox.Show("[GetPartInfo] Line: " + quoteDtlRow.QuoteLine.ToString() + Environment.NewLine + Environment.NewLine + "Error: " + e.Message);
                }
            }

            partBO.Dispose();

            //		Diagnostic
            //		MessageBox.Show("totalVolumePrePackaged = " & totalVolumePrePackaged.ToString() & vbCrLf & _
            //						"TotalQuantityPrePackagedContainers = " & TotalQuantityPrePackagedContainers.ToString() & vbCrLf & _
            //						"totalVolumeNonPrePackaged = " &  totalVolumeNonPrePackaged.ToString() & vbCrLf & _
            //						"totalActualWeight = " & totalActualWeight.ToString())

            partData.TotalVolumePrePackaged = totalVolumePrePackaged;
            partData.TotalQuantityPrePackagedContainers = TotalQuantityPrePackagedContainers;
            partData.TotalVolumeNonPrePackaged = totalVolumeNonPrePackaged;
            partData.TotalActualWeight = totalActualWeight;
            // plp 07-19-2017 --->
            partData.MotorMovementsRelaysProductsOnly = motorMovementsRelaysProductsOnly;
            partData.AnyOversizeProducts = anyOversizeProducts;
            // plp 07-19-2017 <---

            return partData;

        }




    }


    class PartData
    {
        public decimal TotalVolumePrePackaged;
        public int TotalQuantityPrePackagedContainers;
        public decimal TotalVolumeNonPrePackaged;
        public decimal TotalActualWeight;
        public bool MotorMovementsRelaysProductsOnly;
        public bool AnyOversizeProducts;
    }

    #endregion ATQuote


}
