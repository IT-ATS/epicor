﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;
using System;
//Debug info
//ATEpic10T:4022
namespace BpmCustomCode
{
    public class MyOrderDtlTrigger
    {
        private Erp.ErpContext dataContext;
        private Ice.IceContext iceContext;

        public MyOrderDtlTrigger(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
            this.iceContext = (Ice.IceContext)context;
        }

        /// <summary>
        /// updates order release with the mark for and Cap code from the project on the order line
        /// History:
        /// 12/4/17: Return if the cap data is null.  Samage case 3864
        /// 7/6/17:Updated for E10
        /// </summary>
        /// <param name="ttOrderDtl"></param>
        /// <param name="context"></param>
        public void UpdateOrderRelCapCode(System.Collections.Generic.List<Erp.Bpm.TempTables.OrderDtlTempRow> ttOrderDtl, Ice.Tablesets.ContextTableset context)
        {


            foreach (var ttOrderDtlRow in ttOrderDtl)
            {
                if (!string.IsNullOrEmpty(ttOrderDtlRow.ProjectID))
                {
                    var capData = (from p in dataContext.Project
                                    join c in dataContext.Customer on new { p.Company, CustID = p.ShortChar01 } equals new { c.Company, c.CustID }
                                    join st in dataContext.ShipTo on new { c.Company, c.CustNum, ShipToNum = p.ShortChar02 } equals new { st.Company, st.CustNum, st.ShipToNum }
                                    where p.Company == ttOrderDtlRow.Company && p.ProjectID == ttOrderDtlRow.ProjectID
                                    select new
                                    {

                                        c.CustNum,
                                        MarkForNum = p.ShortChar02,
                                        CapCode = st.ShortChar07
                                    }).FirstOrDefault();
                    if (capData == null)
                        return;

                    foreach (var orderRelRow in 
                                                (from row in dataContext.OrderRel.With(LockHint.UpdLock)
                                                where row.Company == ttOrderDtlRow.Company && row.OrderNum == ttOrderDtlRow.OrderNum && row.OrderLine == ttOrderDtlRow.OrderLine
                                                select row))
                    {
                        //get the order release lines with the option to update

                        Int32.TryParse(capData.MarkForNum, out int markFor);
                        orderRelRow.MFCustNum = markFor;
                        orderRelRow.ShortChar07 = capData.CapCode;
                        dataContext.Validate(); //update the db

                    }
                }
            }
        }

        /// <summary>
        /// Updates the comments with the wifi commetns from the project ship to
        /// History:
        /// 9/25/17: No longer update all the comments, just the wifi comments. Clears wifi comments when not needed.  Moved product code test from screen to BPM
        /// 7/6/17:Updated for E10 was OrderDtlComments
        /// 
        /// </summary>
        /// <param name="ttOrderDtl"></param>
        /// <param name="context"></param>
        public void GetWiFiComments(System.Collections.Generic.List<Erp.Bpm.TempTables.OrderDtlTempRow> ttOrderDtl, Ice.Tablesets.ContextTableset context)
        {


            foreach (var ttOrderDtlRow in ttOrderDtl)
            {
                ttOrderDtlRow.Character01 = "";
                if (!string.IsNullOrEmpty(ttOrderDtlRow.ProjectID) && (ttOrderDtlRow.ProdCode.StartsWith("360") || ttOrderDtlRow.ProdCode.StartsWith("260")))
                {
                    var wiFiComment = (from p in dataContext.Project
                                    join c in dataContext.Customer on new { p.Company, CustID = p.ShortChar01 } equals new { c.Company, c.CustID }
                                    join st in dataContext.ShipTo on new { c.Company, c.CustNum, ShipToNum = p.ShortChar02 } equals new { st.Company, st.CustNum, st.ShipToNum }
                                    where p.Company == ttOrderDtlRow.Company && p.ProjectID == ttOrderDtlRow.ProjectID
                                    select new
                                    {
                                        st.ATPrimaryIP,
                                        st.ATSecondIP,
                                        UserCode = st.Character01,
                                        SSID = st.Character07
                                    }).FirstOrDefault();

                    if (wiFiComment == null)
                        return;
                    var userCode = (from uc in iceContext.UDCodes where uc.Company == ttOrderDtlRow.Company && uc.CodeTypeID == "TIMEZONE" && uc.CodeID == wiFiComment.UserCode select uc).FirstOrDefault();

                    string timeZone = "";
                    if (userCode == null)
                        timeZone = "Needed";
                    else
                        timeZone = userCode.CodeDesc;
                    string comment = "";
                    if (!string.IsNullOrEmpty(wiFiComment.ATPrimaryIP) &&
                        !string.IsNullOrEmpty(wiFiComment.ATSecondIP) &&
                        !string.IsNullOrEmpty(wiFiComment.SSID))
                        comment = @"Base Clock Settings ==> Primary Time Server: " + wiFiComment.ATPrimaryIP  +
                                    "  Secondary Time Server: " + wiFiComment.ATSecondIP +
                                    "  TimeZone: " + timeZone +
                                    "  Customer SSID: " + wiFiComment.SSID;

                    ttOrderDtlRow.Character01 = comment;
                }

            }
        }
    }

}