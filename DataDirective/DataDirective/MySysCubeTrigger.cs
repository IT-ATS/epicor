﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;
using System;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace BpmCustomCode
{
    /// <summary>
    /// SysCube is being used to trigger scheduled events 
    /// </summary>
    public class MySysCubeTrigger
    {
        private Erp.ErpContext dataContext;

        public MySysCubeTrigger(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;


        }


        /// <summary>
        /// Sends a shipment email notification based on the email template set in company config.
        /// History:
        /// 1/19/18: Send email to "Other Email" if it is filled in.
        /// 1/11/18: Only send the email if the order is ready to invoice and there is a tracking number  sh.ReadyToInvoice == true && sh.TrackingNumber != ""
        /// 11/2/17: use the entered by email address if the sales rep does not have an email address
        /// 10/17/17: sleep 2 seconds after each email
        /// 7/28/17: updated to include the correct ship via description and cycle on pack id not tracking num
        /// 7/13/17: updated to be scheduled
        /// 6/22/17: Updated for E10 from service to BPM
        /// 8/24/17: Updated to include details of the email that failed.
        /// 9/21/17: Expanded Try catch on emailData to include a larger number of errors.
        /// </summary>
        /// <param name="ttInvcHead"></param>
        /// <param name="context"></param>
        public void SendShipmentEmails(System.Collections.Generic.List<Ice.Bpm.TempTables.SysCubeTempRow> ttSysCube, Ice.Tablesets.ContextTableset context)
        {
            try
            {


                DateTime today = DateTime.Today.AddDays(0);
                //find today's shipments with line detail
                IQueryable<ShipmentEmailData> emailsData = (from sh in dataContext.ShipHead
                                  join sd in dataContext.ShipDtl on new { sh.Company, sh.PackNum } equals new { sd.Company, sd.PackNum }
                                  join od in dataContext.OrderDtl on new { sd.Company, sd.OrderNum, sd.OrderLine } equals new { od.Company, od.OrderNum, od.OrderLine }
                                  join oh in dataContext.OrderHed on new { od.Company, od.OrderNum } equals new { oh.Company, oh.OrderNum }
                                  join cc in dataContext.CustCnt on new { oh.Company, oh.CustNum, ShipToNum = "", ConNum = oh.PrcConNum } equals new { cc.Company, cc.CustNum, cc.ShipToNum, cc.ConNum }
                                  join sr in dataContext.SalesRep on new { oh.Company, SalesRepCode = oh.SalesRepList } equals new { sr.Company, sr.SalesRepCode }
                                  join com in dataContext.Company on new { sh.Company } equals new { Company = com.Company1 }
                                  join u in dataContext.UserFile on new { DcdUserID = oh.EntryPerson } equals new {u.DcdUserID }
                                  where sh.ShipDate == today && sh.ReadyToInvoice == true && sh.TrackingNumber != ""
                                  select new ShipmentEmailData
                                  {
                                      Name = cc.Name,
                                      CustomerEmail = cc.EMailAddress,
                                      OrderNum = oh.OrderNum,
                                      PONum = oh.PONum,
                                      ShipDate = (System.DateTime)sh.ShipDate,
                                      ShipViaCode = sh.ShipViaCode,
                                      TrackingNumber = sh.TrackingNumber,
                                      Qty = sd.SellingInventoryShipQty + sd.SellingJobShipQty,
                                      LineDesc = sd.LineDesc,
                                      PartNum = sd.PartNum,
                                      RepEmail = sr.EMailAddress,
                                      PackNum = sh.PackNum,
                                      PackLine = sd.PackLine,
                                      LiveDB = com.LiveDB_c,
                                      ShipmentEmailSubject_c = com.ShipmentEmailSubject_c,
                                      ShipmentEmail_c = com.ShipmentEmail_c,
                                      UserEmail = u.EMailAddress,
                                      OtherEmail = oh.Character02
                                  });



                //find all the unique packing slips
                var packnums = emailsData.Select(x => x.PackNum).Distinct();
                //send an email for each packing slips
                foreach (var packnum in packnums)
                {

                    //select all the lines for this pack
                    var emailData = (from ed in emailsData where ed.PackNum == packnum select ed);
                    try
                    {


                        var shipVia = emailData.FirstOrDefault().ShipViaCode;
                        //get the description of the shipvia
                        var shipViaDescription = dataContext.ShipVia.Where(s => s.ShipViaCode == shipVia).FirstOrDefault().Description;
                        //get all the packlines
                        var packLines = emailData.Select(x => x.PackLine).Distinct();
                        StringBuilder tableBuilder = new StringBuilder();
                        tableBuilder.Append(@"<table>
                                      <tr>
                                        <th>Quantity</th>
                                        <th>Part Number</th>
                                        <th>Description</th>
                                      </tr>");
                        //cycle through the pack line 
                        foreach (var packLine in packLines)
                        {
                            var line = (from ed in emailData where ed.PackLine == packLine select ed).FirstOrDefault();

                            tableBuilder.AppendFormat(@"  <tr>
                                                <td>{0}</td>
                                                <td>{1}</td>
                                                <td>{2}</td>
                                              </tr>", ((int)line.Qty).ToString(), line.PartNum, line.LineDesc);

                        }
                        tableBuilder.Append(@"</table>");

                        var emailDataBody = emailData.FirstOrDefault();
                        string body = emailDataBody.ShipmentEmail_c;
                        string to = "";
                        string bcc = "";
                        string from = "";
                        string subject = "";
                        StringBuilder debugBuilder = new StringBuilder();


                        if (String.IsNullOrEmpty(emailDataBody.RepEmail))
                            emailDataBody.RepEmail = emailDataBody.UserEmail;
                        if (emailDataBody.LiveDB == true) //only send emails to customers in live
                        {

                            //                    to = "it@atsclock.com";
                            if (String.IsNullOrEmpty(emailDataBody.OtherEmail))
                            {
                                to = emailDataBody.CustomerEmail;
                            }
                            else
                            {

                                to = emailDataBody.OtherEmail;

                            }
                            if (String.IsNullOrEmpty(to))
                                to = emailDataBody.RepEmail;
                            bcc = emailDataBody.RepEmail;
                            from = emailDataBody.RepEmail;
                            subject = emailDataBody.ShipmentEmailSubject_c;

                        }
                        else
                        {
                            to = "it@atsclock.com";
    //                        bcc = emailDataBody.RepEmail;
                            from = emailDataBody.RepEmail;
                            debugBuilder.AppendLine("If this had been in live the following would have been used:");
                            if (String.IsNullOrEmpty(emailDataBody.OtherEmail))
                            {
                                debugBuilder.AppendLine("To Email: " + emailDataBody.CustomerEmail);
                            }
                            else
                            {
                                debugBuilder.AppendLine("To Email: " + emailDataBody.OtherEmail);

                            }

                            subject = "Test - " + emailDataBody.ShipmentEmailSubject_c;


                        }


                        MailDefinition md = new MailDefinition();

                        ListDictionary replacements = new ListDictionary();
                        if (String.IsNullOrEmpty(emailDataBody.OtherEmail))
                        {
                            replacements.Add("{Name}", emailDataBody.Name);
                        }
                        else
                        {
                            replacements.Add("{Name}", "");

                        }
                        replacements.Add("{OrderNum}", emailDataBody.OrderNum.ToString());
                        replacements.Add("{PONum}", emailDataBody.PONum);
                        replacements.Add("{ShipDate}", String.Format("{0:M/d/yyyy}", emailDataBody.ShipDate));
                        replacements.Add("{ShipViaCode}", shipViaDescription);
                        replacements.Add("{RepEmail}", emailDataBody.RepEmail);
                        replacements.Add("{TrackingNumber}", emailDataBody.TrackingNumber);
                        replacements.Add("{Table}", tableBuilder.ToString());
                        replacements.Add("{Debug}", debugBuilder.ToString());

                        if (String.IsNullOrEmpty(from))
                        {
                            from = "atsemails@atsclock.com";
                            bcc = "it@atsclock.com";

                        }
                        md.From = from;

                        MailMessage msg = md.CreateMailMessage(to, replacements, body, new System.Web.UI.Control());

                        var mailer = new Ice.Mail.SmtpMailer(CallContext.Current.Session.CompanyID);
                        var message = new Ice.Mail.SmtpMail();
                        message.SetFrom(from);
                        message.SetTo(to);
                        message.SetSubject(subject);
                        message.SetBody(msg.Body);
                        message.IsBodyHtml = true;
                        message.SetBcc(bcc);
                        mailer.Send(message);
                        System.Threading.Thread.Sleep(2000); // sleep 2 seconds between each email.

                    }
                    catch(Exception ex)
                    {
                        string debug = GetXMLFromObject(emailData.FirstOrDefault());

                        var errormailer = new Ice.Mail.SmtpMailer(CallContext.Current.Session.CompanyID);

                        var errormessage = new Ice.Mail.SmtpMail();

                        errormessage.SetFrom("atsemails@atsclock.com");
                        errormessage.SetTo("it@atsclock.com");
                        errormessage.SetSubject("Error in SendShipmentEmails BPM");

                        errormessage.SetBody(ex.ToString() + Environment.NewLine + debug);
                        errormailer.Send(errormessage);
                    }
                }
            }
            catch (Exception ex)
            {


                var mailer = new Ice.Mail.SmtpMailer(CallContext.Current.Session.CompanyID);

                var message = new Ice.Mail.SmtpMail();

                message.SetFrom("atsemails@atsclock.com");
                message.SetTo("it@atsclock.com");
                message.SetSubject("Error in SendShipmentEmails BPM");
                message.SetBody(ex.ToString());
                mailer.Send(message);

            }
        }


        /// <summary>
        /// History:
        /// 2/8/18: Better email error reporting on 
        /// 10/17/17: sleep 2 seconds after each email
        /// 10/10/17: return if RMAData is null
        /// 7/28/17: Initial release
        /// Close old RMAs or warn that they will be closed if no response is recieved.
        /// </summary>
        /// <param name="ttSysCube"></param>
        /// <param name="context"></param>
        public void ScheduledRMAClose(System.Collections.Generic.List<Ice.Bpm.TempTables.SysCubeTempRow> ttSysCube, Ice.Tablesets.ContextTableset context)
        {

            var RMAData = GetRMADetails();
            if (RMAData == null)
                return;
            try
            {
                //cycle through all the rma
                foreach (var RMA in RMAData)
                {


                    int daysToClose = (RMA.CloseDate - DateTime.Today).Days;    //see how old they are
                    if (daysToClose == 10)   //send a warning email and write a case entry
                    {
                        //Create a call log indicating an email was sent to the customer
                        #region CreateCallLog
                        var crmCallBo = Ice.Assemblies.ServiceRenderer.GetService<Erp.Contracts.CRMCallSvcContract>(dataContext);
                        var crmCallDS = new Erp.Tablesets.CRMCallTableset();
                        crmCallBo.GetNewCRMCall(ref crmCallDS, "rmahead", RMA.RMANum.ToString(), "", "");
                        var newCallRow = (from c in crmCallDS.CRMCall where c.RowMod == "A" select c).FirstOrDefault();
                        newCallRow.CallDesc = "Sent email";
                        newCallRow.CallText = "Emailed Customer and Customer Service Representative that the RMA is still open and will be closed in 10 days if not received from the client";
                        newCallRow.SalesRepCode = RMA.CaseOwner;
                        newCallRow.CallTypeCode = "EMAIL";

                        crmCallBo.Update(ref crmCallDS);

                        crmCallBo.Dispose();
                        #endregion CreateCallLog

                        //send an email to the customer if in live otherwise send the email to it
                        #region EmailCustomer

                        string to = "";
                        string bcc = "";
                        string from = "";
                        string subject = "";
                        StringBuilder debugBuilder = new StringBuilder();
                        if (RMA.LiveDB == true) //only send emails to customers in live
                        {
                            to = RMA.CustomerEmail;
                            bcc = RMA.RepEmailAddress;
                            from = RMA.RepEmailAddress;
                            subject = RMA.WarningEmailSubject;

                        }
                        else
                        {
                            to = "it@atsclock.com";
                            bcc = RMA.RepEmailAddress;
                            from = RMA.RepEmailAddress;
                            debugBuilder.AppendLine("If this had been in live the following would have been used:");
                            debugBuilder.AppendLine("To Email: " + RMA.CustomerEmail);
                            subject = "Test - " + RMA.WarningEmailSubject;


                        }

                        MailDefinition md = new MailDefinition();
                        //replace the items in the tempalte with data
                        ListDictionary replacements = new ListDictionary();
                        replacements.Add("{Name}", RMA.CustomerName);
                        replacements.Add("{RMANum}", RMA.RMANum.ToString());
                        replacements.Add("{RepEmail}", RMA.RepEmailAddress);
                        replacements.Add("{Debug}", debugBuilder.ToString());

                        md.From = from;

                        MailMessage msg = md.CreateMailMessage(to, replacements, RMA.WarningEmail, new System.Web.UI.Control());

                        var mailer = new Ice.Mail.SmtpMailer(CallContext.Current.Session.CompanyID);
                        var message = new Ice.Mail.SmtpMail();
                        message.SetFrom(from);
                        message.SetTo(to);
                        message.SetCC(bcc);
                        message.SetSubject(subject);
                        message.SetBody(msg.Body);
                        message.IsBodyHtml = true;
                        message.SetBcc(bcc);
                        mailer.Send(message);
                        #endregion EmailCustomer

                    }
                    else if (daysToClose <= 0)  //send a email and close the RMA
                    {


                        //Close the RMA
                        #region CloseRMA

                        var rMAProcBo = Ice.Assemblies.ServiceRenderer.GetService<Erp.Contracts.RMAProcSvcContract>(dataContext);
                        var rmaDS = rMAProcBo.GetByID(RMA.RMANum);
                        rmaDS.RMAHead[0].OpenRMA = false;
                        bool userInputRequired;
                        rMAProcBo.PreUpdate(ref rmaDS, out userInputRequired);
                        if (userInputRequired)
                        {
                            var errormailer = new Ice.Mail.SmtpMailer(CallContext.Current.Session.CompanyID);
                            var errormessage = new Ice.Mail.SmtpMail();
                            errormessage.SetFrom("atsemails@atsclock.com");
                            errormessage.SetTo("it@atsclock.com");
                            errormessage.SetSubject("Unable to close RMA");
                            errormessage.SetBody("RMA " + RMA.RMANum.ToString() + " requires user input and could not be closed.");
                            errormailer.Send(errormessage);

                        }

                        rmaDS.RMAHead[0].RowMod = "U";
                        rMAProcBo.Update(ref rmaDS);
                        rmaDS = rMAProcBo.GetByID(RMA.RMANum);
                        if (rmaDS.RMAHead[0].OpenRMA == true)
                        {
                            var errormailer = new Ice.Mail.SmtpMailer(CallContext.Current.Session.CompanyID);
                            var errormessage = new Ice.Mail.SmtpMail();
                            errormessage.SetFrom("atsemails@atsclock.com");
                            errormessage.SetTo("it@atsclock.com");
                            errormessage.SetSubject("Unable to close RMA");
                            errormessage.SetBody("Tried to close RMA " + RMA.RMANum.ToString() + " but it stayed open");
                            errormailer.Send(errormessage);
                            return;

                        }
                        rMAProcBo.Dispose();


                        #endregion CloseRMA

                        //create a call log
                        #region CreateCallLog
                        var crmCallBo = Ice.Assemblies.ServiceRenderer.GetService<Erp.Contracts.CRMCallSvcContract>(dataContext);
                        var crmCallDS = new Erp.Tablesets.CRMCallTableset();
                        crmCallBo.GetNewCRMCall(ref crmCallDS, "rmahead", RMA.RMANum.ToString(), "", "");
                        var newCallRow = (from c in crmCallDS.CRMCall where c.RowMod == "A" select c).FirstOrDefault();
                        newCallRow.CallDesc = "Sent email";
                        newCallRow.CallText = "Emailed Customer and Owner that the RMA/Case is closed";
                        newCallRow.SalesRepCode = RMA.CaseOwner;
                        newCallRow.CallTypeCode = "EMAIL";
                        try
                        {
                            crmCallBo.Update(ref crmCallDS);
                            var helpDeskBo = Ice.Assemblies.ServiceRenderer.GetService<Erp.Contracts.HelpDeskSvcContract>(dataContext);

                            helpDeskBo.PreOpenCloseCase(RMA.HDCaseNum, true, out string epimessage);
                            helpDeskBo.OpenCloseCase(RMA.HDCaseNum, true);

                        }
                        catch (Exception ex)
                        {
                            string debug = GetXMLFromObject(RMA);

                            var errormailer = new Ice.Mail.SmtpMailer(CallContext.Current.Session.CompanyID);

                            var errormessage = new Ice.Mail.SmtpMail();
                            errormessage.SetFrom("atsemails@atsclock.com");
                            errormessage.SetTo("it@atsclock.com");
                            errormessage.SetSubject("Error in ScheduledRMAClose BPM");
                            errormessage.SetBody(ex.ToString() + Environment.NewLine + "RMANum: " + RMA.RMANum.ToString() + Environment.NewLine + debug);
                            errormailer.Send(errormessage);
                            return;

                        }

                        #endregion CreateCallLog


                        //Send the customer an email
                        #region SendEmail
                        string to = "";
                        string bcc = "";
                        string from = "";
                        string subject = "";
                        StringBuilder debugBuilder = new StringBuilder();
                        if (RMA.LiveDB == true) //only send emails to customers in live
                        {
                            to = RMA.CustomerEmail;
                            bcc = RMA.RepEmailAddress;
                            from = RMA.RepEmailAddress;
                            subject = RMA.CloseEmailSubject;

                        }
                        else
                        {
                            to = "it@atsclock.com";
                            from = RMA.RepEmailAddress;
                            debugBuilder.AppendLine("If this had been in live the following would have been used:");
                            debugBuilder.AppendLine("To Email: " + RMA.CustomerEmail);
                            debugBuilder.AppendLine("BCC Email: " + RMA.RepEmailAddress);
                            subject = "Test - " + RMA.CloseEmailSubject;


                        }



                        MailDefinition md = new MailDefinition();

                        ListDictionary replacements = new ListDictionary();
                        replacements.Add("{Name}", RMA.CustomerName);
                        replacements.Add("{RMANum}", RMA.RMANum.ToString());
                        replacements.Add("{RepEmail}", RMA.RepEmailAddress);
                        replacements.Add("{Debug}", debugBuilder.ToString());

                        md.From = from;

                        MailMessage msg = md.CreateMailMessage(to, replacements, RMA.CloseEmail, new System.Web.UI.Control());


                        //   md.Subject = "Test of MailDefinition";


                        var mailer = new Ice.Mail.SmtpMailer(CallContext.Current.Session.CompanyID);

                        var message = new Ice.Mail.SmtpMail();

                        message.SetFrom(from);


                        message.SetTo(to);
                        message.SetCC(bcc);

                        message.SetSubject(subject);


                        message.SetBody(msg.Body);
                        message.IsBodyHtml = true;
                        message.SetBcc(bcc);

                        mailer.Send(message);
                        System.Threading.Thread.Sleep(2000); // sleep 2 seconds between each email.

                        #endregion SendEmail

                    }


                }

            }

            catch (Exception ex)
            {


                string debug = GetXMLFromObject(RMAData.FirstOrDefault());
                var mailer = new Ice.Mail.SmtpMailer(CallContext.Current.Session.CompanyID);
                var message = new Ice.Mail.SmtpMail();
                message.SetFrom("atsemails@atsclock.com");
                message.SetTo("it@atsclock.com");
                message.SetSubject("Error in ScheduledRMAClose BPM");
                message.SetBody(ex.ToString() + Environment.NewLine + debug);
                mailer.Send(message);

            }
        }

        /// <summary>
        /// turns an object into xml for emailing error
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }
        /// <summary>
        /// Gets all open RMAs without reciepts 
        /// </summary>
        /// <returns></returns>
        IQueryable<RMADataDef> GetRMADetails()
        {
            var rmaData = (from rh in dataContext.RMAHead
                           join rroj in dataContext.RMARcpt on new { rh.Company, rh.RMANum } equals new { rroj.Company, rroj.RMANum }
                                   into rrs
                           from rr in rrs.DefaultIfEmpty()
                           join c in dataContext.HDCase on new { rh.Company, rh.HDCaseNum } equals new { c.Company, c.HDCaseNum }
                           join sr in dataContext.SalesRep on new { c.Company, SalesRepCode = c.CaseOwner } equals new { sr.Company, sr.SalesRepCode }
                           join com in dataContext.Company on new { rh.Company } equals new { Company = com.Company1 }
                           join sa in dataContext.SaleAuth on new { sr.Company, sr.SalesRepCode } equals new { sa.Company, sa.SalesRepCode }
                           where rh.OpenRMA == true && rr == null && c.HDCaseStatus == "OPEN" && sa.DcdUserID == "ATSManager"
                           select new RMADataDef
                           {
                               RMANum = rh.RMANum,
                               CloseDate = (System.DateTime)rh.Date01,
                               CustomerName = c.Character03,
                               CustomerEmail = c.Character04,
                               RepEmailAddress = sr.EMailAddress,
                               WarningEmailSubject = com.RMAWarningEmailSubject_c,
                               WarningEmail = com.RMAWarningEmail_c,
                               CloseEmailSubject = com.RMACloseEmailSubject_c,
                               CloseEmail = com.RMACloseEmail_c,
                               LiveDB = com.LiveDB_c,
                               HDCaseNum = c.HDCaseNum,
                               CaseOwner = c.CaseOwner,
                               Company = rh.Company
                           });
            return rmaData;
        }
    }

    /// <summary>
    /// Definse the shipment Email object
    /// </summary>
    public class ShipmentEmailData
    {
        public string Name { get; set; }

        public string CustomerEmail { get; set; }
        public int OrderNum { get; set; }
        public string PONum { get; set; }
        public System.DateTime ShipDate { get; set; }
        public string ShipViaCode { get; set; }
        public string TrackingNumber { get; set; }
        public decimal Qty { get; set; }
        public string LineDesc { get; set; }
        public string PartNum { get; set; }
        public string RepEmail { get; set; }
        public int PackNum { get; set; }
        public int PackLine { get; set; }
        public bool LiveDB { get; set; }
        public string ShipmentEmailSubject_c { get; set; }
        public string ShipmentEmail_c { get; set; }
        public string UserEmail { get; set; }
        public string OtherEmail { get; set; }


    }



    /// <summary>
    /// Definse the RMA object
    /// </summary>
    public class RMADataDef
    {
        public int RMANum { get; set; }

        public System.DateTime CloseDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string RepEmailAddress { get; set; }
        public string WarningEmailSubject { get; set; }
        public string WarningEmail { get; set; }
        public string CloseEmailSubject { get; set; }
        public string CloseEmail { get; set; }
        public bool LiveDB { get; set; }
        public int HDCaseNum { get; set; }
        public string CaseOwner { get; set; }
        public string Company { get; set; }

    }

}

