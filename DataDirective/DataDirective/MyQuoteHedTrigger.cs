﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;
using System;
using System.IO;

namespace BpmCustomCode
{
    public class MyQuoteHedTrigger
    {
        private Erp.ErpContext dataContext;

        public MyQuoteHedTrigger(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
        }

        /// <summary>
        /// Appends the customer comments stored in Character01 to the quote comments
        /// History:
        /// 1/1/18: Copied from orderhed
        /// </summary>
        /// <param name="ttQuoteHed"></param>
        /// <param name="context"></param>
        public void GetCustomerComments(System.Collections.Generic.List<Erp.Bpm.TempTables.QuoteHedTempRow> ttQuoteHed, Ice.Tablesets.ContextTableset context)
        {


            var quoteHedRow = (from r in ttQuoteHed
                               where r.RowMod == "A" || r.RowMod == "U"
                               select r).FirstOrDefault();

            if (quoteHedRow == null)
                return;
            var customerRow = (from row in dataContext.Customer where row.CustNum == quoteHedRow.CustNum && row.Company == quoteHedRow.Company select row).FirstOrDefault();
            if (customerRow == null)
                return;
            if (customerRow.Character01.Length > 0)
            {
                quoteHedRow.QuoteComment = customerRow.Character01 + Environment.NewLine + quoteHedRow.QuoteComment;
            }

        }
    }
}

