﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;

namespace BpmCustomCode
{
    public class MyPOHeaderTrigger
    {
        private Erp.ErpContext dataContext;

        public MyPOHeaderTrigger(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
        }
        /// <summary>
        /// Defaults the comments from the supplier on a new PO. 
        /// History:
        /// 9/7/17:Initial release
        /// </summary>
        /// <param name="ttPOHeader"></param>
        /// <param name="context"></param>
        public void DefaultComments(System.Collections.Generic.List<Erp.Bpm.TempTables.POHeaderTempRow> ttPOHeader, Ice.Tablesets.ContextTableset context)
        {


            var POHeaderRow = (from r in ttPOHeader
                               where r.RowMod == "A"
                               select r).FirstOrDefault();

            if (POHeaderRow == null)
                return;
            string comment = (from row in dataContext.Vendor where row.VendorNum == POHeaderRow.VendorNum && row.Company == POHeaderRow.Company select row.POComments_c).FirstOrDefault();
            if (!string.IsNullOrEmpty(comment))
            {
                if (string.IsNullOrEmpty(POHeaderRow.CommentText))
                    POHeaderRow.CommentText = comment;
            }
        }
    }
}

