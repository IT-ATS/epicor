﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ATS
{
    /// <summary>
    /// Definition of a clock part number based on the Clock Number Sequencing	spread sheet
    /// History
    /// 7/12/17 Initial release CWR
    /// </summary>
    class ClockNumberSequencing
    {

        public ClockNumberSequencing(string _partNumber)
        {
            this.PartNumber = _partNumber;
        }

        public Boolean ValidPart;
        string partNumber;
        /// <summary>
        /// The clock part number seting the part number will update all all the clock parameters
        /// </summary>
        public string PartNumber
        {
            get
            {
                return partNumber;
            }
            set
            {

                partNumber = value;
                BuildParameters(partNumber);
            }
        }

        string productType;
        public string ProductType
        {
            get
            {
                return productType;
            }
            set
            {
                throw new NotImplementedException();
                productType = value;
            }
        }


        string productSize;
        public string ProductSize
        {
            get
            {
                return productSize;
            }
            set
            {
                throw new NotImplementedException();
                productSize = value;
            }
        }

        string productMounting;
        public string ProductMounting
        {
            get
            {
                return productMounting;
            }
            set
            {
                throw new NotImplementedException();
                productMounting = value;
            }
        }

        string bezel;
        public string Bezel
        {
            get
            {
                return bezel;
            }
            set
            {
                throw new NotImplementedException();
                bezel = value;
            }
        }

        string movementOrVoltage;
        public string MovementOrVoltage
        {
            get
            {
                return movementOrVoltage;
            }
            set
            {
                throw new NotImplementedException();
                movementOrVoltage = value;
            }
        }

        string correctionOrFrequency;
        public string CorrectionOrFrequency
        {
            get
            {
                return correctionOrFrequency;
            }
            set
            {
                throw new NotImplementedException();
                correctionOrFrequency = value;
            }
        }

        string hanger;
        public string Hanger
        {
            get
            {
                return hanger;
            }
            set
            {
                throw new NotImplementedException();
                hanger = value;
            }
        }

        string dialAndColor;
        public string DialAndColor
        {
            get
            {
                return dialAndColor;
            }
            set
            {
                throw new NotImplementedException();
                dialAndColor = value;
            }
        }

        string customerDesignation;
        public string CustomerDesignation
        {
            get
            {
                return CustomerDesignation;
            }
            set
            {
                throw new NotImplementedException();
                CustomerDesignation = value;
            }
        }


        string oTAClockType;
        public string OTAClockType
        {
            get
            {
                return oTAClockType;
            }
        }

        /// <summary>
        /// Build the clock parameters from the given part number
        /// </summary>
        /// <param name="partnumber"></param>
        private void BuildParameters(string partnumber)
        {


            productType = Regex.Match(partnumber, "(^[A-Z]+)").Value;
            string p = partnumber.Substring(productType.Length);    //the remaing part number after the product type
            productSize = Regex.Match(p, @"^\d").Value; //get the first diget
            productMounting = Regex.Match(p, @"(?<=^.{1})\d").Value; //get the second digit
            bezel = Regex.Match(p, @"(?<=^.{2})[A-Z]{2}").Value; //get the next 2 characters
            movementOrVoltage = Regex.Match(p, @"(?<=^.{4})[A-Z]").Value; //get the next character
            correctionOrFrequency = Regex.Match(p, @"(?<=^.{5})[A-Z]").Value; //get the next character
            hanger = Regex.Match(p, @"(?<=^.{6}).").Value; //get the next alpha numeric character
            dialAndColor = Regex.Match(p, @"(?<=^.{7})..").Value; //get the next 2 alpha numeric characters
            customerDesignation = Regex.Match(p, @"(?<=^.{9}).*").Value; //get the remaining characters


            ValidPart = true;

            if (String.IsNullOrEmpty(productType))
                ValidPart = false;
            if (String.IsNullOrEmpty(productSize))
                ValidPart = false;
            if (String.IsNullOrEmpty(productMounting))
                ValidPart = false;
            if (String.IsNullOrEmpty(bezel))
                ValidPart = false;
            if (String.IsNullOrEmpty(movementOrVoltage))
                ValidPart = false;
            if (String.IsNullOrEmpty(correctionOrFrequency))
                ValidPart = false;
            if (String.IsNullOrEmpty(hanger))
                ValidPart = false;
            if (String.IsNullOrEmpty(customerDesignation))
                ValidPart = false;

            if (productType == "WNA")
            {
                oTAClockType = "06";
            }
            else if (productType == "WN")
            {
                if (movementOrVoltage == "A" || movementOrVoltage == "B")
                    oTAClockType = "04";
                else if (movementOrVoltage == "D")
                    oTAClockType = "03";
                else
                    oTAClockType = "01";

            }
            else
            {
                oTAClockType = "01";

            }


        }


    }
}
