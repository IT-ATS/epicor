﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;

namespace BpmCustomCode
{
    public class MyRMAHeadTrigger
    {
        private Erp.ErpContext dataContext;

        public MyRMAHeadTrigger(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
        }
        /// <summary>
        /// Updates the RMAHed.ShortChar01 with the project id on the case.
        /// Fields modified RMAHed.ShortChar01
        /// </summary>
        /// <param name="ttRMAHead"></param>
        /// <param name="context"></param>
        public void UpdateProjectFromCase(System.Collections.Generic.List<Erp.Bpm.TempTables.RMAHeadTempRow> ttRMAHead, Ice.Tablesets.ContextTableset context)
        {
            var rmaHedRow = (from r in ttRMAHead
                               where r.RowMod == "A" || r.RowMod == "U" && r.OpenRMA == true
                               select r).FirstOrDefault();
            //did we find one?
            if (rmaHedRow == null || rmaHedRow.HDCaseNum == 0)
                return;

            rmaHedRow.ShortChar01 = (from row in dataContext.HDCase where row.HDCaseNum == rmaHedRow.HDCaseNum && row.Company == rmaHedRow.Company select row.ProjectID).FirstOrDefault();



        }
    }
}

