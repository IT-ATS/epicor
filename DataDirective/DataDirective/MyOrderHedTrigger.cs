﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;
using System;
using System.IO;
using ATS;

namespace BpmCustomCode
{
    public class MyOrderHedTrigger
    {
        private Erp.ErpContext dataContext;

        public MyOrderHedTrigger(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
        }


        /// <summary>
        /// Defaults the invoicing email address on a new order if the address is blank
        /// </summary>
        /// <param name="ttOrderHed"></param>
        /// <param name="context"></param>
        public void DefaultInvoiceEmail(System.Collections.Generic.List<Erp.Bpm.TempTables.OrderHedTempRow> ttOrderHed, Ice.Tablesets.ContextTableset context)
        {
            //is our order new with no email address?
            var orderHedRow = (from r in ttOrderHed
                               where r.RowMod == "A" && r.InvoiceEmail_c == ""
                               select r).FirstOrDefault();
            //did we find one?
            if (orderHedRow == null)
                return;

            orderHedRow.InvoiceEmail_c = ATS.CommonFunctions.DefaultEmail(orderHedRow.Company, orderHedRow.CustNum, 0, dataContext); 



        }


        /// <summary>
        /// Updates the customer record with the last order date.
        /// Business purpose:  Unknown
        /// History:
        /// 12/7/17:Bug fix: check if customerRow.Date04 is null and update it. Prior to this new customers were not getting their first order date updated.
        /// 6/30/17:Updated for E10 CWR
        /// </summary>
        /// <param name="ttOrderHed"></param>
        /// <param name="context"></param>
        public void UpdateCustomerLastOrderDate(System.Collections.Generic.List<Erp.Bpm.TempTables.OrderHedTempRow> ttOrderHed, Ice.Tablesets.ContextTableset context)
        {
            //find our current order
            var orderHedRow = (from r in ttOrderHed
                               where r.RowMod == "A" || r.RowMod == "U"
                               select r).FirstOrDefault();
            //did we find one?
            if (orderHedRow == null)
                return;
            //get the current customer with the option to update it.
            var customerRow = (from row in dataContext.Customer.With(LockHint.UpdLock) where row.CustNum == orderHedRow.CustNum && row.Company == orderHedRow.Company select row).FirstOrDefault();

            if (customerRow == null)
                return;
            //is the current last order date less than the last one
            if (customerRow.Date04 < orderHedRow.OrderDate || customerRow.Date04 == null)
            {
                
                customerRow.Date04 = orderHedRow.OrderDate;// updte the last order date
                dataContext.Validate(); //update the db
            }

        }


        /// <summary>
        /// Appends the customer comments stored in Character01 to the Order comments
        /// History:
        /// 6/20/17:Updated for E10
        /// </summary>
        /// <param name="ttOrderHed"></param>
        /// <param name="context"></param>
        public void GetCustomerComments(System.Collections.Generic.List<Erp.Bpm.TempTables.OrderHedTempRow> ttOrderHed, Ice.Tablesets.ContextTableset context)
        {



            var orderHedRow = (from r in ttOrderHed
                               where r.RowMod == "A" || r.RowMod == "U"
                               select r).FirstOrDefault();

            if (orderHedRow == null)
                return;
            var customerRow = (from row in dataContext.Customer where row.CustNum == orderHedRow.CustNum && row.Company == orderHedRow.Company select row).FirstOrDefault();
            if (customerRow == null)
                return;
            if (customerRow.Character01.Length > 0)
            {
                orderHedRow.OrderComment = customerRow.Character01 + Environment.NewLine + orderHedRow.OrderComment;
            }
        }
        /// <summary>
        /// Gets the sales person from the quote and sales it to the order
        /// History:
        /// 6/20/17:Updated for E10
        /// </summary>
        /// <param name="ttOrderHed"></param>
        /// <param name="context"></param>
        public void GetQuoteSalesperson(System.Collections.Generic.List<Erp.Bpm.TempTables.OrderHedTempRow> ttOrderHed, Ice.Tablesets.ContextTableset context)
        {


            var orderHedRow = (from r in ttOrderHed
                               where r.RowMod == "A" || r.RowMod == "U"
                               select r).FirstOrDefault();
            if (orderHedRow == null)
                return;
            if (orderHedRow.ShortChar02.Length < 1 || orderHedRow.ShortChar03.Length < 1)   //only update once.
            {

                var orderDtlRow = (from r in dataContext.OrderDtl where r.OrderNum == orderHedRow.OrderNum && r.Company == orderHedRow.Company && r.OrderLine == 1 select r).FirstOrDefault();
                if (orderDtlRow == null)
                    return;

                var quoteHedRow = (from r in dataContext.QuoteHed where r.QuoteNum == orderDtlRow.QuoteNum && r.Company == orderDtlRow.Company select r).FirstOrDefault();
                if (quoteHedRow == null)
                    return;

                if (quoteHedRow.ShortChar02.Length > 0)
                    orderHedRow.ShortChar02 = quoteHedRow.ShortChar02;

                if (quoteHedRow.ShortChar03.Length > 0)
                    orderHedRow.ShortChar03 = quoteHedRow.ShortChar03;
            }

        }
        /// <summary>
        /// Creates the OTA clock settings file 
        /// History:
        /// 8/8/17: added product group check and has wifi check
        /// 7/12/17:Updated for new clock types.  Uses ClockNumberSequencing
        /// 6/20/17:Updated for E10
        /// </summary>
        /// <param name="ttOrderHed"></param>
        /// <param name="context"></param>
        public void CreateClockSettingsFile(System.Collections.Generic.List<Erp.Bpm.TempTables.OrderHedTempRow> ttOrderHed, Ice.Tablesets.ContextTableset context)
        {
            var orderHedRow = (from r in ttOrderHed
                               where r.RowMod == "A" || r.RowMod == "U"
                               select r).FirstOrDefault();
            if (orderHedRow == null)
                return;
            
            var companyRow = (from r in dataContext.Company where  r.Company1 == orderHedRow.Company  select r).FirstOrDefault();
            if (companyRow == null)
                return;

            //            string filePath = @"\\ATFILE01\manufacturing$\WiFi\GS2000\Not_Done\E10Test\";
            string filePath = companyRow.OTAFileLocation_c;



            var orderDtlRows = (from r in dataContext.OrderDtl where r.OrderNum == orderHedRow.OrderNum && r.Company == orderHedRow.Company select r).ToList();
            if (orderDtlRows == null)
                return;
            foreach (var orderDtlRow in orderDtlRows)
            {
                if (orderDtlRow.ProjectID.Length > 0 && (orderDtlRow.ProdCode.StartsWith("360") || orderDtlRow.ProdCode.StartsWith("260")))
                {
                    var projectRow = (from r in dataContext.Project where r.ProjectID == orderDtlRow.ProjectID && r.Company == orderDtlRow.Company select r).FirstOrDefault();
                    if (projectRow != null)
                    {
                        var customerRow = (from r in dataContext.Customer where r.CustID == projectRow.ShortChar01 && r.Company == projectRow.Company select r).FirstOrDefault();
                        if (customerRow != null)
                        {
                            var shipToRow = (from r in dataContext.ShipTo where r.CustNum == customerRow.CustNum && r.ShipToNum == projectRow.ShortChar02 && r.Company == customerRow.Company select r).FirstOrDefault();
                            if (shipToRow != null)
                            {
                                if (shipToRow.WifiCheckBox)
                                {

                                    ClockNumberSequencing clock = new ClockNumberSequencing(orderDtlRow.PartNum);
                                
                                    using (StreamWriter writer = new StreamWriter(filePath + orderDtlRow.OrderNum.ToString() + "-" + orderDtlRow.OrderLine.ToString() + ".txt"))
                                    {
                                        writer.WriteLine(shipToRow.ATChannel);                    /*  Channel  */
                                        writer.WriteLine(shipToRow.Character01);                        /*  Time Zone */
                                        writer.WriteLine(shipToRow.Character02);                        /*  DST  */

    //used a fixed ota clock type for now.
                                        writer.WriteLine("01");                      /*  Clock Type   */

                                        //                                    writer.WriteLine(clock.OTAClockType);                      /*  Clock Type   */
                                        writer.WriteLine(shipToRow.ATPrimaryIP);              /*  Pri Time Server IP  */
                                        writer.WriteLine(shipToRow.ATSecondIP);                      /*  Sec Time Server IP  */
                                        writer.WriteLine(shipToRow.Character06);                        /*  Security Mode  */
                                        writer.WriteLine(shipToRow.Character07);                        /*  Customer SSID  */
                                        writer.WriteLine(shipToRow.Character08);                        /*  Customer SSID PW  */
                                        writer.WriteLine("192168010245");                      /*  ATS Pri Time Server ID  */
                                        writer.WriteLine("064147116229");                       /*  ATS Sec Time Server ID  */
                                        writer.WriteLine(shipToRow.Character09);                        /*  WEP Auth Mode  */
                                        writer.WriteLine(shipToRow.Character10);                        /*  WEP Key Indx  */
                                        writer.WriteLine(shipToRow.ATWEPKey);                        /*  WEP Key  */
                                        writer.WriteLine(shipToRow.ATEAPType);                        /*  EAP Type  */
                                        writer.WriteLine(shipToRow.ATEAPUser);                       /*  EAP UserName  */
                                        writer.WriteLine(shipToRow.ATEAPPass);                       /*  EAP Password  */
                                        writer.WriteLine(shipToRow.ATStartMonth);    /*  Start Month  */
                                        writer.WriteLine(shipToRow.ATStartHour);  /*  Start Hour  */
                                        writer.WriteLine(shipToRow.ATStartMinute); /*  Start Minute  */
                                        writer.WriteLine(shipToRow.ATStartSecond); /*  Start Second  */
                                        writer.WriteLine(shipToRow.ATStopMonth);  /*  Stop Month  */
                                        writer.WriteLine(shipToRow.ATStopHour);    /*  Stop Hour  */
                                        writer.WriteLine(shipToRow.ATStopMinute);  /*  Stop Minute  */
                                        writer.WriteLine(shipToRow.ATStopSecond);    /*  Stop Second  */
                                        writer.WriteLine(shipToRow.ShortChar08);      /*  Bias Sign  */
                                        writer.WriteLine(shipToRow.ATBiasHours);  /*  Bias Hours    */
                                        writer.WriteLine(shipToRow.ATBiasMinute);    /*  Bias Minutes  */
                                        writer.WriteLine(shipToRow.ATStartDay);      /*  Start Day  */
                                        writer.WriteLine(shipToRow.ATStopDay);        /*  Stop Day  */
                                        writer.WriteLine(shipToRow.ATStartWeek);  /*  Start Day of Week  */
                                        writer.WriteLine(shipToRow.ATStartWkNum);    /*  Start Week Num  */
                                        writer.WriteLine(shipToRow.ATStopWeek);    /*  Stop Day of Week  */
                                        writer.WriteLine(shipToRow.ATStopWkNum);  /*  Stop Week Num  */
                                        writer.WriteLine(shipToRow.ATProUser);                        /*  Provisioning Entry User  */
                                        writer.WriteLine(shipToRow.ATProPass);                        /*  Provisioning Entry Password  */
                                        if (shipToRow.SNTPDNS == "2")
			                                writer.WriteLine("1");
                                        else
                            			    writer.WriteLine(shipToRow.SNTPDNS);                       /*  SNTP DNS Flag  */
                                        writer.WriteLine(shipToRow.TimeServerType);              /*  SNTP Pool Domain Name  */
                                        writer.WriteLine(customerRow.CustID);                               /*  Custommer ID  */
                                        writer.WriteLine(orderHedRow.OrderNum);       /*  Order Number  */
                                        writer.WriteLine(shipToRow.ATComments);                      /*  Comments  */
                                    }
                                }
                            }


                        }
                    }

                }
            }

        }

    }
}