﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;

namespace BpmCustomCode
{
    public class MyInvcHeadTrigger
    {
        private Erp.ErpContext dataContext;

        public MyInvcHeadTrigger(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
        }


        /// <summary>
        /// Defaults InvoiceEmail_c from the order.  If the order doesn't have a email address it defaults from the customer. 
        /// 
        /// </summary>
        /// <param name="ttInvcHead"></param>
        /// <param name="context"></param>
        public void DefaultInvoiceEmail(System.Collections.Generic.List<Erp.Bpm.TempTables.InvcHeadTempRow> ttInvcHead, Ice.Tablesets.ContextTableset context)
        {
            var invcHeadRow = (from r in ttInvcHead
                               where r.InvoiceEmail_c == ""
                               select r).FirstOrDefault();
            //did we find one?
            if (invcHeadRow == null)
                return;


            invcHeadRow.InvoiceEmail_c = ATS.CommonFunctions.DefaultEmail(invcHeadRow.Company, invcHeadRow.CustNum, invcHeadRow.OrderNum, dataContext);

        }
    }
}

