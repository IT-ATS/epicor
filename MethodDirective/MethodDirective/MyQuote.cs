﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;
using Erp.Proxy.BO;
using System;
using Erp.BO;
using Ice;

namespace BpmCustomCode
{
    public class MyQuote
    {
        private Erp.ErpContext dataContext;

        public MyQuote(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
        }

        /// <summary>
        /// Updates the Checkbox01 in the newly created order with the value of Checkbox01
        /// to indicate the misc line was created by the freight calc
        /// Fields modified OHOrderMsc.CheckBox01
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="orderNum"></param>
        /// <param name="warningMessage"></param>
        /// <param name="context"></param>
        public void UpdateOrderCheckBox01(
           Erp.Tablesets.QuoteTableset ds,
           ref System.Int32 orderNum,
           ref System.String warningMessage,
           Ice.Tablesets.ContextTableset context)
        {
            try
            {
                using (var salesOrderImpl = Ice.Assemblies.ServiceRenderer.GetService<Erp.Contracts.SalesOrderSvcContract>(dataContext))
                {


                    var salesOrderDs = salesOrderImpl.GetByID(orderNum);

                    int quoteNum = 0;
                    quoteNum = (from row in salesOrderDs.OrderDtl.AsEnumerable() where row.QuoteNum != 0 select row.QuoteNum).FirstOrDefault();

                    if (quoteNum != 0)
                    {

                        using (var quoteBo = Ice.Assemblies.ServiceRenderer.GetService<Erp.Contracts.QuoteSvcContract>(dataContext))
                        {

                            var quoteDs = quoteBo.GetByID(quoteNum);


                            var quoteHedMscRow = (from row in quoteDs.QuoteHedMsc.AsEnumerable() where (bool)row["CheckBox01"] == true select row).FirstOrDefault();
                            if (quoteHedMscRow != null)
                            {
                                int seqNum = quoteHedMscRow.SeqNum;

                                var orderHedMscRow = (from row in salesOrderDs.OHOrderMsc.AsEnumerable() where (int)row["SeqNum"] == seqNum select row).FirstOrDefault();

                                if (orderHedMscRow != null)
                                {
                                    orderHedMscRow.RowMod = "U";
                                    orderHedMscRow["CheckBox01"] = true;
                                    salesOrderImpl.Update(ref salesOrderDs);

                                }

                            }

                        }

                    }


                }



            }
            catch (Exception ex)
            {
            }



        }

  




        /// <summary>
        /// Duplicates extra fields when copying a quote.
        /// Removes the extra sales person when duplicating a quote
        /// Fields modified:
        /// QuoteHed.BestCsPct
        /// QuoteHed.WorstCsPct
        /// </summary>
        /// <param name="sourceQuote"></param>
        /// <param name="sourceLines"></param>
        /// <param name="mtlCosts"></param>
        /// <param name="opStds"></param>
        /// <param name="result"></param>
        /// <param name="context"></param>
        public void FixUpNewQuote(
           ref System.Int32 sourceQuote,
           ref System.String sourceLines,
           ref System.Boolean mtlCosts,
           ref System.Boolean opStds,
           Erp.Tablesets.QuoteTableset result,
           Ice.Tablesets.ContextTableset context)
        {

                using (var quoteBo = Ice.Assemblies.ServiceRenderer.GetService<Erp.Contracts.QuoteSvcContract>(dataContext))
                {


                    int newQuoteNum = (from r in result.QuoteHed

                                       select r.QuoteNum).FirstOrDefault();
                    var quoteDS = quoteBo.GetByID(newQuoteNum);


                    //Removes the extra sales rep that may appear when duplicating a quote
                    #region RemoveExtraSalesRep


                    var ttQSalesRP = quoteDS.QSalesRP;
                    var ttQSalesRPRow = (from r in ttQSalesRP
                                         where r.RepSplit == 0
                                         select r).FirstOrDefault(); // the extra record shoudl have a split of 0

                    if (ttQSalesRPRow == null)
                        return;


                    if (ttQSalesRP.Count > 1)   //do we at least have two records
                    {
                        ttQSalesRPRow.RowMod = "D";
                    }


                    #endregion RemoveExtraSalesRep


                    //Duplicates extra fields when copying a quote
                    #region DuplicateFields

                    int oldQuoteNum = sourceQuote;
                    string currentCompany = context.Client[0].CurrentCompany;

                    var oldQuoteHed = (from q in dataContext.QuoteHed
                                       where q.Company == currentCompany && q.QuoteNum == oldQuoteNum
                                       select q).FirstOrDefault();

                    var ttQuoteHedRow = quoteDS.QuoteHed[0];

                    ttQuoteHedRow.BestCsPct = oldQuoteHed.BestCsPct;
                    ttQuoteHedRow.WorstCsPct = oldQuoteHed.WorstCsPct;
                    ttQuoteHedRow.RowMod = "U";
                    quoteBo.Update(ref quoteDS);


                    #endregion DuplicateFields

                }

        }


    }
}

