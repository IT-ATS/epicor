﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;
using Ice;
using System;

namespace BpmCustomCode
{
    public class MyProject
    {
        private Erp.ErpContext dataContext;

        public MyProject(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
        }
        /// <summary>
        /// Get's the shipto info from the customer shipto. display's a hard stop if the shipto is invalid.
        /// Fields modified:Project.Character02
        /// 
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="context"></param>
        public void GetShipToInfo(Erp.Tablesets.ProjectTableset ds, Ice.Tablesets.ContextTableset context)
        {
            var ttProject = ds.Project;
            var ttProjectRow = (from r in ttProject
                                where r.RowMod == "A" || r.RowMod == "U"
                                select r).FirstOrDefault();

            if (ttProjectRow == null)
                return;

            string custID = ttProjectRow["ShortChar01"].ToString();
            string shipToNum = ttProjectRow["ShortChar02"].ToString();

            var shipToInfo = (from c in dataContext.Customer 
                          join st in dataContext.ShipTo on new { c.Company, c.CustNum} equals new { st.Company, st.CustNum }
                          where c.Company == ttProjectRow.Company && c.CustID == custID && st.ShipToNum == shipToNum
                              select new
                          {
                              st.Name,
                              st.Address1,
                              st.City,
                              st.State,
                              st.ZIP,
                              st.ShortChar07,
                              st.Number03

                          }).FirstOrDefault();



            if (shipToInfo != null)
            {
                ttProjectRow["Character02"] = shipToInfo.Name + "\n" + shipToInfo.Address1 + "\n" + shipToInfo.City + ", " + shipToInfo.State + "  " + shipToInfo.ZIP + "\n\nCAP Code: " + shipToInfo.ShortChar07 + "\nFrequency: " + shipToInfo.Number03.ToString("0.###");
            }
            else
            {
                if (!String.IsNullOrEmpty(custID) || !String.IsNullOrEmpty(shipToNum))
                {
                    var message =
                   "The Customer Site ID and/or the Site ShipTo are not found.  Please correct.\r\n\r\nBPM";

                    throw new Ice.Common.BusinessObjectException(
                        new Ice.Common.BusinessObjectMessage(message)
                        {
                            Type = Ice.Common.BusinessObjectMessageType.Error,
                        });
                }
                ttProjectRow["Character02"] = "";
            }
        }
    }
}