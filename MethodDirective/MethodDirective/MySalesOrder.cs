﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;

namespace BpmCustomCode
{
    public class MySalesOrder
    {
        private Erp.ErpContext dataContext;

        public MySalesOrder(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
        }

        public void DefaultInvoiceEmail(Erp.Tablesets.SalesOrderTableset ds, Ice.Tablesets.ContextTableset context)
        {

            var ttOrderHed = ds.OrderHed;
            var ttOrderHedRow = (from r in ttOrderHed
                                where r.RowMod == "A" || r.RowMod == "U"
                                select r).FirstOrDefault();

            if (ttOrderHedRow == null)
                return;

            ttOrderHedRow["InvoiceEmail_c"] = ATS.CommonFunctions.DefaultEmail(ttOrderHedRow.Company, ttOrderHedRow.CustNum, ttOrderHedRow.OrderNum, dataContext);

        }
    }
}

