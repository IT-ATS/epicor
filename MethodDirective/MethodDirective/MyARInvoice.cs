﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;

namespace BpmCustomCode
{
    public class MyARInvoice
    {
        private Erp.ErpContext dataContext;

        public MyARInvoice(Ice.IceDataContext context)
        {
            // 
            // TODO: should not be disposed, as custom assembly does not own the context
            // 
            this.dataContext = (Erp.ErpContext)context;
        }

        public void DefaultInvoiceEmail(
           ref System.Int32 InvoiceNum,
           ref System.String NewCustomerID,
           Erp.Tablesets.ARInvoiceTableset ds,
           Ice.Tablesets.ContextTableset context)
        {

            var ttInvcHead = ds.InvcHead;
            var ttInvcHeadRow = (from r in ttInvcHead
                                 where r.RowMod == "A" || r.RowMod == "U"
                                 select r).FirstOrDefault();

            if (ttInvcHeadRow == null)
                return;

            ttInvcHeadRow["InvoiceEmail_c"] = ATS.CommonFunctions.DefaultEmail(ttInvcHeadRow.Company, ttInvcHeadRow.CustNum, ttInvcHeadRow.OrderNum, dataContext);
        }


        public void DefaultInvoiceEmail(
           ref System.Int32 InvoiceNum,
           ref System.Int32 NewOrderNum,
           Erp.Tablesets.ARInvoiceTableset ds,
           Ice.Tablesets.ContextTableset context)
        {
            var ttInvcHead = ds.InvcHead;
            var ttInvcHeadRow = (from r in ttInvcHead
                                 where r.RowMod == "A" || r.RowMod == "U"
                                 select r).FirstOrDefault();

            if (ttInvcHeadRow == null)
                return;

            ttInvcHeadRow["InvoiceEmail_c"] = ATS.CommonFunctions.DefaultEmail(ttInvcHeadRow.Company, ttInvcHeadRow.CustNum, ttInvcHeadRow.OrderNum, dataContext);
        }



        public void DefaultInvoiceEmail(
           ref System.String NewSoldToCustID,
           Erp.Tablesets.ARInvoiceTableset ds,
           Ice.Tablesets.ContextTableset context)
        {
            string custID = NewSoldToCustID;
            var ttInvcHead = ds.InvcHead;
            var ttInvcHeadRow = (from r in ttInvcHead
                                 where r.RowMod == "A" || r.RowMod == "U" && r.SoldToCustID == custID
                                 select r).FirstOrDefault();

            if (ttInvcHeadRow == null)
                return;

            ttInvcHeadRow["InvoiceEmail_c"] = ATS.CommonFunctions.DefaultEmail(ttInvcHeadRow.Company, ttInvcHeadRow.CustNum, ttInvcHeadRow.OrderNum, dataContext);
        }

    }
}

