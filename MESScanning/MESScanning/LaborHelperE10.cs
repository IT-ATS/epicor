﻿#define Debug
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Data;
using Ice.Core;
using Ice.Lib.Framework;
using Erp.Proxy.BO;
using Erp.BO;

namespace CRCHelper
{

    #region LaborHelper

    /*History
     *         /// 20171011: Clean up on memory leaks

     * 20171020: Added propt to to continue if there is a warning.
     * Enabled the display of shop warnings when clocking in.9/5/17 CWR
     * Don't clock in if complete
     * 
     * 
     * */

    /// <summary>
    /// Class to help with creating labor transactions
    /// Usage:
    /// Session epiSession = new Session("user","password", "AppServerDC:/EpicorServer:9401", Session.LicenseType.Default);
    /// lh = new LaborHelper(epiSession, statusLabel);
    /// Dependencies:EmpBasic, JobOperSearch, JobProdSearch, Labor
    /// </summary>
    class LaborHelper : IDisposable
    {

        /// History:
        /// 20171011: allow for subasemblies with multiple qty.
        /// 20170508: Initial release
        /// </summary>
        string version = "20171011";


        Session epiSession;
        LaborImpl laborBO;
        EmpBasicImpl empBasicBO;
        JobOperSearchImpl jobOperSearchBO;
        JobProdSearchImpl jobProdSearchBO;
        JobEntryImpl jobBO;

        string employeeID = "";
        public LaborHelper(Session _epiSession, Label statusLabel)
        {


            //          statusLabel.Text += " LaborHelper:" + version;
            epiSession = _epiSession;
            laborBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.LaborImpl>(epiSession, Erp.Proxy.BO.LaborImpl.UriPath);
            empBasicBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.EmpBasicImpl>(epiSession, Erp.Proxy.BO.EmpBasicImpl.UriPath);
            jobOperSearchBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.JobOperSearchImpl>(epiSession, Erp.Proxy.BO.JobOperSearchImpl.UriPath);
            jobProdSearchBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.JobProdSearchImpl>(epiSession, Erp.Proxy.BO.JobProdSearchImpl.UriPath);
            jobBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.JobEntryImpl>(epiSession, Erp.Proxy.BO.JobEntryImpl.UriPath);

        }


        /// <summary>
        /// Clean up all the bo that we grabbed
        /// </summary>
        public void Dispose()
        {
            laborBO.Dispose();
            empBasicBO.Dispose();
            jobOperSearchBO.Dispose();
            jobProdSearchBO.Dispose();
            jobBO.Dispose();
            laborBO = null;
            empBasicBO = null;
            jobOperSearchBO = null;
            jobProdSearchBO = null;
            jobBO = null;

        }

        /// <summary>
        /// Finds the current employee.  If one isn't available prompts to enter the employee id
        /// </summary>
        public void FindEmployee()
        {
            if (epiSession.EmployeeID == "" || epiSession.EmployeeID == null)
            {
                string value = "";
                if (InputBox("Employee", "Enter Employee ID:", ref value) == DialogResult.OK)
                {
                    employeeID = value;
                }
            }
            else
            {
                employeeID = epiSession.EmployeeID;
            }

        }

        /// <summary>
        /// End a production labor activity
        /// </summary>
        /// <param name="jobNum">The job number</param>
        /// <param name="assemblySeq">The assembly sequence</param>
        /// <param name="op">the op code</param>
        /// <param name="finishQty">If true the entire qty will be finished.</param>
        /// <returns></returns>
        public bool EndProduction(string jobNum, int assemblySeq, string op, bool finishQty)
        {

            string laborHedWhere = "EmployeeNum = '" + employeeID + "' and ActiveTrans = yes BY EmployeeNum";
            string laborDtlWhere = "JobNum = '" + jobNum + "' and ActiveTrans = 'yes' and OpCode = '" + op.ToString() + "'";
            bool b;
            LaborDataSet laborDS = laborBO.GetRows(laborHedWhere, laborDtlWhere, "", "", "", "", "", "", "", "", "", "", 0, 1, out b);
            int laborHedSeq = Convert.ToInt32(laborDS.Tables["LaborHed"].Rows[0]["LaborHedSeq"].ToString());

            if (laborDS.Tables["LaborDtl"].Rows.Count != 0) //we are clocked in
            {
                int laborDtlSeq = Convert.ToInt32(laborDS.Tables["LaborDtl"].Rows[0]["LaborDtlSeq"].ToString());
                laborDS = laborBO.GetDetail(laborHedSeq, laborDtlSeq);
                laborDS.Tables["LaborDtl"].Rows[0]["EndActivity"] = true;
                laborDS.Tables["LaborDtl"].Rows[0]["RowMod"] = "U";
                if (finishQty)
                {
                    JobProdListDataSet jobProdListDS = jobProdSearchBO.GetList("JobNum = '" + jobNum + "'", 0, 0, out b);
                    laborDS.Tables["LaborDtl"].Rows[0]["LaborQty"] = Convert.ToInt32(jobProdListDS.Tables["JobProdList"].Rows[0]["ProdQty"].ToString());
                    laborDS.Tables["LaborDtl"].Rows[0]["OpComplete"] = true;

                    string vMessage = "";
                    laborBO.EndActivityComplete(laborDS, true, out vMessage);

                }
                else
                {
                    laborBO.EndActivity(laborDS);

                }

                laborBO.Update(laborDS);

            }

            return true;
        }

        /// <summary>
        /// End a production labor activity
        /// </summary>
        /// <param name="jobNum">The job number</param>
        /// <param name="assemblySeq">The assembly sequence</param>
        /// <param name="opSeq">the operation sequence</param>
        /// <param name="finishQty">If true the entire qty will be finished.</param>
        /// <returns></returns>
        public bool EndProductionSeq(string jobNum, int assemblySeq, int opSeq, bool finishQty)
        {

            string laborHedWhere = "EmployeeNum = '" + employeeID + "' and ActiveTrans = yes BY EmployeeNum";
            string laborDtlWhere = "JobNum = '" + jobNum + "' and ActiveTrans = 'true' and OprSeq = '" + opSeq.ToString() + "'";
            bool b;
            LaborDataSet laborDS = laborBO.GetRows(laborHedWhere, laborDtlWhere, "", "", "", "", "", "", "", "", "", "", 0, 1, out b);
            int laborHedSeq = Convert.ToInt32(laborDS.Tables["LaborHed"].Rows[0]["LaborHedSeq"].ToString());

            if (laborDS.Tables["LaborDtl"].Rows.Count != 0) //we are clocked in
            {
                int laborDtlSeq = Convert.ToInt32(laborDS.Tables["LaborDtl"].Rows[0]["LaborDtlSeq"].ToString());
                laborDS = laborBO.GetDetail(laborHedSeq, laborDtlSeq);
                laborDS.Tables["LaborDtl"].Rows[0]["EndActivity"] = true;
                laborDS.Tables["LaborDtl"].Rows[0]["RowMod"] = "U";
                if (finishQty)
                {
                    var jobDS = jobBO.GetByID(jobNum);
                    JobEntryDataSet.JobOperRow operRow = (JobEntryDataSet.JobOperRow)(from o in jobDS.JobOper.AsEnumerable()
                                   where (int)o["AssemblySeq"] == assemblySeq && (int)o["OprSeq"] == opSeq
                                   select o).FirstOrDefault();
                    laborDS.Tables["LaborDtl"].Rows[0]["LaborQty"] = operRow.ProductionQty - operRow.QtyCompleted;
                    laborDS.Tables["LaborDtl"].Rows[0]["OpComplete"] = true;

                    string vMessage = "";
                    laborBO.EndActivityComplete(laborDS, true, out vMessage);

                }
                else
                {
                    laborBO.EndActivity(laborDS);

                }

                laborBO.Update(laborDS);

            }

            return true;
        }


        /// <summary>
        /// Starts a production activity.  If the person isn't clocked in it will clock them in for the day
        /// </summary>
        /// <param name="jobNum">The Job number</param>
        /// <param name="assemblySeq">the assembly sequence </param>
        /// <param name="op">the op code</param>
        /// <returns></returns>
        public bool StartProduction(string jobNum, int assemblySeq, string op)
        {
            if (!ClockIn())
            {
                MessageBox.Show("Unable to clock in.");
                return false;
            }
            bool b;
            try
            {

                string laborHedWhere = "EmployeeNum = '" + employeeID + "' and ActiveTrans = yes BY EmployeeNum";
                MessageBox.Show("Need to fix filter on opcode");
                string laborDtlWhere = "JobNum = '" + jobNum + "' and ActiveTrans = 'yes'";

                LaborDataSet laborDS = laborBO.GetRows(laborHedWhere, laborDtlWhere, "", "", "", "", "", "", "", "", "", "", 0, 1, out b);
                int laborHedSeq = Convert.ToInt32(laborDS.Tables["LaborHed"].Rows[0]["LaborHedSeq"].ToString());

                if (laborDS.Tables["LaborDtl"].Rows.Count == 0) //we aren;t already clocked in
                {
                    laborBO.StartActivity(laborHedSeq, "P", laborDS);
                    laborBO.DefaultJobNum(laborDS, jobNum);
                    laborBO.DefaultAssemblySeq(laborDS, assemblySeq);
                    string vMessage = "";
                    laborBO.DefaultOpCode(laborDS, op, out vMessage);
                    if (!String.IsNullOrEmpty(vMessage))
                        MessageBox.Show(vMessage);

                    JobOperListDataSet jobOperListDS = jobOperSearchBO.GetList("JobNum = '" + jobNum + "'", 0, 0, out b);
                    foreach (DataRow row in jobOperListDS.Tables["JobOperList"].Rows)
                    {
                        if (row["OpCode"].ToString() == op)
                        {
                            laborBO.DefaultOprSeq(laborDS, Convert.ToInt32(row["OprSeq"].ToString()), out vMessage);
                            if (!String.IsNullOrEmpty(vMessage))
                            {

                                var result = MessageBox.Show(vMessage + Environment.NewLine + "Would you like to continue?", "Continue?", MessageBoxButtons.YesNo);
                                if (result == DialogResult.No || result == DialogResult.Cancel)
                                {
                                    return false;
                                }

                            }

                        }

                    }
                    laborBO.Update(laborDS);

                }

                return true;

            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// Starts a production activity.  If the person isn't clocked in it will clock them in for the day
        /// </summary>
        /// <param name="jobNum">The Job number</param>
        /// <param name="assemblySeq">the assembly sequence </param>
        /// <param name="opSeq">the operation sequence</param>
        /// <returns></returns>
        public bool StartProductionSeq(string jobNum, int assemblySeq, int opSeq)
        {
            if (!ClockIn())
            {
                MessageBox.Show("Unable to clock in.");
                return false;
            }
            bool b;
            try
            {

                string laborHedWhere = "EmployeeNum = '" + employeeID + "' and ActiveTrans = yes BY EmployeeNum";
                string laborDtlWhere = "JobNum = '" + jobNum + "' and ActiveTrans = 'true' and OprSeq = '" + opSeq.ToString() + "'";

                LaborDataSet laborDS = laborBO.GetRows(laborHedWhere, laborDtlWhere, "", "", "", "", "", "", "", "", "", "", 0, 1, out b);
                int laborHedSeq = Convert.ToInt32(laborDS.Tables["LaborHed"].Rows[0]["LaborHedSeq"].ToString());

                if (laborDS.Tables["LaborDtl"].Rows.Count == 0) //we aren;t already clocked in
                {
                    laborBO.StartActivity(laborHedSeq, "P", laborDS);
                    laborBO.DefaultJobNum(laborDS, jobNum);
                    laborBO.DefaultAssemblySeq(laborDS, assemblySeq);
                    string vMessage = "";
                    laborBO.DefaultOprSeq(laborDS, opSeq, out vMessage);
                    if (!String.IsNullOrEmpty(vMessage))
                    {

                        var result = MessageBox.Show(vMessage + Environment.NewLine + "Would you like to continue?", "Continue?", MessageBoxButtons.YesNo);
                        if (result == DialogResult.No || result == DialogResult.Cancel)
                        {
                            return false;
                        }

                    }

                    laborBO.Update(laborDS);

                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// Finds and clocks in the current employee
        /// </summary>
        /// <returns></returns>
        public bool ClockIn()
        {
            try
            {

                FindEmployee();
                bool b;
                LaborDataSet laborDS = laborBO.GetRows("EmployeeNum = '" + employeeID + "' and ActiveTrans = yes BY EmployeeNum", "", "", "", "", "", "", "", "", "", "", "", 0, 0, out b);
                if (laborDS.Tables["LaborHed"].Rows.Count == 0)
                {
                    empBasicBO.ClockIn(employeeID, 1);
                }
                return true;
            }
            catch
            {
                return false;

            }
        }
        /// <summary>
        /// Generic input box
        /// </summary>
        /// <param name="title"></param>
        /// <param name="promptText"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new System.Drawing.Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new System.Drawing.Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

    }
    #endregion LaborHelper

}
