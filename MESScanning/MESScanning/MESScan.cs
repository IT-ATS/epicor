﻿#define Debug

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ice.Core;
using Ice.Lib.Framework;
using Erp.Proxy.BO;
using Erp.BO;
using CRCHelper;
using System.Windows.Forms;

namespace MESScanning
{


    #region MESScan


    /// <summary>
    /// History:
	/// 20171003: Always convert the barcode to upper before reading. Added Dispose
    /// 20171002: Updated to prompt if it shoudl continue on warnings.
    /// 
    /// </summary>

    class MESScan : IDisposable
    {

        /// History:
        /// 20171019: added second option for scanner termination character
        /// 20171011: Clean up on memory leaks
        /// 201709188: Initial release
        /// </summary>
        string version = "20171019";



        /// <summary>
        /// helper class for reading the barcodes
        /// </summary>
        MESBarcodeScan bcs;
        /// <summary>
        /// Helper for labor transactions
        /// </summary>
        LaborHelper lh;

        Form prompt;
        Session epiSession;

        /// <summary>
        /// reads keypresses from a barcode scnner and automates based on teh info passed
        /// non scanned keypresses are passed to the form
        /// </summary>
        /// <param name="_epiSession">the current epicor session</param>
        /// <param name="_panel">the base MES panel</param>
        Label statusLabel;

        public MESScan(Session _epiSession)
        {
            epiSession = _epiSession;
            /*statusLabel = _statusLabel;
            statusLabel.Text = "Waiting for input.";

            lh = new LaborHelper(_epiSession, statusLabel);
            bcs = new MESBarcodeScan(_panel, "\r", statusLabel);

            bcs.ScanFinished += new EventHandler(bcs_ScanFinished);
            */
        }
        public void Dispose()
        {
            if (lh != null)
            {
                lh.Dispose();
                lh = null;
            }
        }


        public DialogResult ShowDialog()
        {
            prompt = new Form()
            {
                Width = 800,
                Height = 300,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = "Waiting for Barcode Scan",
                StartPosition = FormStartPosition.CenterScreen
            };
            statusLabel = new Label();
            statusLabel.Left = 50;
            statusLabel.Top = 20;
            statusLabel.Width = 800;
            statusLabel.Text = "Waiting for input.";

            lh = new LaborHelper(epiSession, statusLabel);
            bcs = new MESBarcodeScan(prompt, "\r", "\t", statusLabel);

            bcs.ScanFinished += new EventHandler(bcs_ScanFinished);

            //            Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 50, DialogResult = DialogResult.OK };
            //          confirmation.Click += new System.EventHandler(confirmation_Click);
            //            confirmation.Height = 40;
            //confirmation.TabStop = false;

            //  prompt.Controls.Add(confirmation);
            prompt.Controls.Add(statusLabel);


            prompt.KeyPreview = true;


            //           prompt.AcceptButton = confirmation;
            prompt.ShowDialog();


            bcs.ScanFinished -= new EventHandler(bcs_ScanFinished);
            lh.Dispose();
            bcs.Dispose();

            return DialogResult.OK;
        }


        /// <summary>
        /// event when a barcode scan is complete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bcs_ScanFinished(object sender, EventArgs e)
        {
            ScanFinishedEventArgs s = (ScanFinishedEventArgs)e;
            CheckBarCodeInput(s.barcodeInput);
        }

        /// <summary>
        /// checks the inptu fromt the barcode scanner
        /// </summary>
        /// <param name="barcodeInput">the string send fromt he barcode scanner</param>
        public void CheckBarCodeInput(string barcodeInput)
        {
            barcodeInput = barcodeInput.ToUpper();
            string[] commands = barcodeInput.Split('.');    //commands are seperated by a . to maintane code 39 compatiblity
            string opcode = commands[0];
            if (opcode == "A")   //end the operation completing it
            {
                string jobNum = commands[1];
                int asmSeq = 0;
                int opSeq = 0;
                int.TryParse(commands[2], out asmSeq);
                int.TryParse(commands[3], out opSeq);
                bool startProductionSeqOK = false;
                bool endProductionSeqOK = false;

                statusLabel.Text = "Clocking into Job:" + jobNum + " Asm:" + asmSeq.ToString() + " Op:" + opSeq.ToString();
                prompt.Refresh();
                startProductionSeqOK = lh.StartProductionSeq(jobNum, asmSeq, opSeq); //clock in
                if (startProductionSeqOK)
                {
                    statusLabel.Text = "Clocking out of Job:" + jobNum + " Asm:" + asmSeq.ToString() + " Op:" + opSeq.ToString();
                    prompt.Refresh();
                    endProductionSeqOK = lh.EndProductionSeq(jobNum, asmSeq, opSeq, true); //and out finishing the qty.

                }
                if (startProductionSeqOK == false)
                {
                    statusLabel.Text = "Error clocking into Job:" + jobNum + " Asm:" + asmSeq.ToString() + " Op:" + opSeq.ToString();

                }
                else if (endProductionSeqOK == false)
                {
                    statusLabel.Text = "Error clocking out of Job:" + jobNum + " Asm:" + asmSeq.ToString() + " Op:" + opSeq.ToString();

                }
                else
                {
                    statusLabel.Text = "Sucessfully completed Job:" + jobNum + " Asm:" + asmSeq.ToString() + " Op:" + opSeq.ToString();

                }
                prompt.Refresh();


            }
        }

    }
    #endregion MESScan


}
