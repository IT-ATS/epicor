﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CRCHelper;
using Ice.Core;

namespace MESScanning
{
    public partial class Form1 : Form
    {
        MESBarcodeScan bcs;
        MESScan ms;
        LaborHelper lh;
    Session epiSession;
        public Form1()
        {
            InitializeComponent();

            string configFile = @"C:\Epicor\ERP10.1ClientTest\Client\config\EpicorTest10.sysconfig";
            //string configFile = @"C:\Epicor\ERP10.1Client\Client\config\EpicorLive10.sysconfig";
            epiSession = new Session("manager", "manager", Session.LicenseType.DataCollection, configFile);
            epiSession.EmployeeID = "300";
            /*            if (!epiSession.CompanyName.Contains("TEST"))
                        {
                            MessageBox.Show("WARING!!!You are in live.");
                        }
                        */
            ms = new MESScan(epiSession);

            lh = new LaborHelper(epiSession, label1);
//            bcs = new BarcodeScan(this, "\r");

//            bcs.ScanFinished += new EventHandler(bcs_ScanFinished);


        }
        private void bcs_ScanFinished(object sender, EventArgs e)
        {
            ScanFinishedEventArgs s = (ScanFinishedEventArgs)e;
            barcodeinputtb.Text =   s.barcodeInput;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ms.ShowDialog();

//            ms.CheckBarCodeInput(SampleScan.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lh.StartProductionSeq("883994-1-1", 0, 10);
            lh.EndProductionSeq("883994-1-1", 0, 10, true);
        }
    }
}
