﻿#define Debug

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.ComponentModel;
using System.IO;
using System.Data;

namespace CRCHelper
{


    #region MESBarcodeScan



    class MESBarcodeScan : IDisposable
    {

        /// History:
        /// 20171019: Added a secdon option for a termination character
        /// 20170508: Initial release
        /// </summary>
        string version = "20171019";


        public event EventHandler ScanFinished;
        bool resendKeys;
        string terminationChar1 = "";
        string terminationChar2 = "";
        string barcodeInput = "";
        Timer barcodeTimer;

        /// <summary>
        /// captures incoming key presses on a panel.  if they come in fast enough then assue it's a barcode scan and fire an external event passing the scnned text
        /// if it comes in slow pass the key pres on forward.
        /// </summary>
        /// <param name="_panel">the panel that we are going to capture keypresses from </param>
        /// <param name="_terminationChar1">the character the scanner is programed to end with  </param>
        /// <param name="_terminationChar2">the character the scanner is programed to end with  </param>
        Form panel;
        public MESBarcodeScan(Form _panel, string _terminationChar1, string _terminationChar2, Label statusLabel)
        {

            panel = _panel;

            panel.KeyPreview = true;
            // statusLabel.Text += " MESBarcodeScan:" + version;

            panel.KeyPress += new KeyPressEventHandler(panel_KeyDown);
            terminationChar1 = _terminationChar1;
            terminationChar2 = _terminationChar2;
        }

        public void Dispose()
        {
            panel.KeyPress += new KeyPressEventHandler(panel_KeyDown);

        }
        /// <summary>
        /// catch all key presses on this panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel_KeyDown(object sender, KeyPressEventArgs e)
        {
            if (!resendKeys)
            {
                ReadKeyPress(e);
                e.Handled = true;
            }
            resendKeys = false;


        }
        /// <summary>
        /// reads the key presses on this panel
        /// hopefully they are from a barcode scanner
        /// sets a timer to ensure they are from a scanner
        /// </summary>
        /// <param name="key"></param>
        public void ReadKeyPress(KeyPressEventArgs key)
        {
            if (panel.ContainsFocus == true)
            {

                if (barcodeTimer == null)
                {
                    barcodeTimer = new Timer();
                    barcodeTimer.Interval = 100;
                    barcodeTimer.Tick += new EventHandler(barcodeTimer_Tick);
                }
                barcodeTimer.Stop();
                barcodeTimer.Start();
                if (key.KeyChar.ToString() == terminationChar1 || key.KeyChar.ToString() == terminationChar2) //we got the termination character so this must be the end
                {
                    barcodeTimer.Enabled = false;
                    if (barcodeInput.Length > 4)
                    {
                        ScanFinishedEventArgs scanFinishedArgs = new ScanFinishedEventArgs(barcodeInput);

                        if (this.ScanFinished != null)  //throw an event so the form can be loaded
                            this.ScanFinished(this, scanFinishedArgs);

                    }
                    barcodeInput = "";


                }
                else
                    barcodeInput += key.KeyChar;
            }

        }

        /// <summary>
        /// The key inputs came too slow.
        /// Clear out the barcode input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void barcodeTimer_Tick(object sender, EventArgs e)
        {

            resendKeys = true;
            if (panel.ContainsFocus == true)    //only send the keys on if we still have focus
                SendKeys.Send(barcodeInput);
            barcodeInput = "";
            barcodeTimer.Stop();
        }
    }

    public class ScanFinishedEventArgs : EventArgs
    {
        public ScanFinishedEventArgs(string barcodeInput)
        {
            this.barcodeInput = barcodeInput;
        }
        public string barcodeInput;

    }


    #endregion MESBarcodeScan


}
