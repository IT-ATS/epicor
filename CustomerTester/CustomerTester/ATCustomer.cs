﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ice.Core;
using Erp.Proxy.BO;
using Ice.Proxy.BO;
using Ice.Lib.Framework;
using Erp.BO;
using Ice.BO;
using Newtonsoft.Json.Linq;

namespace CustomerTester
{

    #region ATCustomer

    static class ATCustomer
    {

        public static void SetWiFiReadonly(CustomerDataSet.ShipToRow shipTo, ref bool aTPrimaryIPReadOnly, ref bool aTSecondIPReadOnly, ref bool timeServerTypeReadOnly)
        {
            switch (shipTo["SNTPDNS"].ToString())
            {

                case "0":   //static IP
                    aTPrimaryIPReadOnly = false;
                    aTSecondIPReadOnly = false;
                    timeServerTypeReadOnly = true;
                    break;
                case "1": //customer supplied DNS
                    aTPrimaryIPReadOnly = true;
                    aTSecondIPReadOnly = true;
                    timeServerTypeReadOnly = false;
                    break;
                case "2":   //default
                    aTPrimaryIPReadOnly = true;
                    aTSecondIPReadOnly = true;
                    timeServerTypeReadOnly = true;


                    break;

            }

        }
        public static void SetWiFiFields(CustomerDataSet.ShipToRow shipTo, Session epiSession)
        {
            switch (shipTo["SNTPDNS"].ToString())
            {

                case "0":   //static IP
                    shipTo["TimeServerType"] = GetSNTPPool(epiSession);
                    break;
                case "1": //customer supplied DNS
                    shipTo["ATPrimaryIP"] = "0.0.0.0";
                    shipTo["ATSecondIP"] = "0.0.0.0";
                    break;
                case "2":   //default
                    shipTo["ATPrimaryIP"] = "0.0.0.0";
                    shipTo["ATSecondIP"] = "0.0.0.0";
                    shipTo["TimeServerType"] = GetSNTPPool(epiSession);
                    break;



            }
        }
        public static string GetSNTPPool(Session epiSession)
        {
            string timeServerType = "";
            using (UserCodesImpl userCodesBO = WCFServiceSupport.CreateImpl<Ice.Proxy.BO.UserCodesImpl>(epiSession, Ice.Proxy.BO.UserCodesImpl.UriPath))
            {
                bool b;
                var userCodesDS = userCodesBO.GetRows("CodeTypeID = 'SNTPPool'", "", 0, 0, out b);

                Random r = new Random();

                int row = r.Next(0, userCodesDS.UDCodes.Count);

                timeServerType = userCodesDS.UDCodes[row].CodeDesc;

            }
            return timeServerType;

        }

        /// <summary>
        /// Checks the if the private check box should be checked uncheck and if it shoudl be enabled
        /// </summary>
        /// <param name="customer">a customer data row</param>
        /// <param name="epiSession"></param>
        /// <returns>the enabled only value if the checkbox</returns>
        public static bool CheckPublicPrivate(CustomerDataSet.CustomerRow customer, Session epiSession)
        {
            var defaultCredit = GetUserJSON("CUSTTYPE", customer["ShortChar02"].ToString(), epiSession);

            if (defaultCredit["PublicPrivateChoice"] != null)
            {
                if ((string)defaultCredit["PublicPrivateChoice"] == "Private")
                {
                    customer["Private_c"] = true;
                    customer.RowMod = "U";

                    return false;
                }
                else if ((string)defaultCredit["PublicPrivateChoice"] == "Public")
                {
                    customer["Private_c"] = false;
                    customer.RowMod = "U";

                    return false;
                }
            }
            return true;

        }

        /// <summary>
        /// Defaults the customer credit based on the customer type
        /// </summary>
        /// <param name="customer">a customer data row</param>
        /// <param name="epiSession"></param>
        public static void DefaultCustomerCredit(CustomerDataSet.CustomerRow customer, Session epiSession)
        {

            var defaultCredit = GetUserJSON("CUSTTYPE", customer["ShortChar02"].ToString(), epiSession);

            if (defaultCredit["CreditCardOrder"] != null)
            {
                customer.CreditCardOrder = (bool)defaultCredit["CreditCardOrder"];
            }
            if (defaultCredit["PublicPrivateChoice"] != null)
            {
                if ((string)defaultCredit["PublicPrivateChoice"] == "Private")
                {
                    customer["Private_c"] = true;

                }
                else if ((string)defaultCredit["PublicPrivateChoice"] == "Public")
                {
                    customer["Private_c"] = false;

                }

                if ((bool)customer["Private_c"])
                {
                    //                    MessageBox.Show(customer["Private_c"].ToString());
                    if (defaultCredit["PrivateCreditLimit"] != null)
                    {
                        customer.CreditLimit = (int)defaultCredit["PrivateCreditLimit"];

                    }
                }
                else
                {
                    if (defaultCredit["PublicCreditLimit"] != null)
                    {
                        customer.CreditLimit = (int)defaultCredit["PublicCreditLimit"];

                    }

                }

            }

            if (defaultCredit["TermsCode"] != null)
            {
                customer.TermsCode = (string)defaultCredit["TermsCode"];
                customer.CreditHold = false;
                customer.RowMod = "U";
            }

        }


        /// <summary>
        /// Defaults the bank and payment fields from the terms.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="epiSession"></param>
        public static void DefaultBankAndPaymentFromTerms(CustomerDataSet.CustomerRow customer, Session epiSession)
        {
            if (customer == null)
                return;
            string terms = customer.TermsCode;
            if (String.IsNullOrEmpty(terms))
                return;

            TermsImpl termsBO = WCFServiceSupport.CreateImpl<TermsImpl>(epiSession, TermsImpl.UriPath);

            try
            {
                var termsDs = termsBO.GetByID(terms);
                customer.PreferredBank = (string)termsDs.Terms[0]["PreferredBank_c"];
                customer.PMUID = (int)termsDs.Terms[0]["PMUID_c"];
                customer.RowMod = "U";
            }
            catch { }

            termsBO.Dispose();





        }


        /// <summary>
        /// Reads the JSON encoded data from the given user code
        /// </summary>
        /// <param name="CodeTypeID"></param>
        /// <param name="CodeID"></param>
        /// <param name="epiSession"></param>
        /// <returns></returns>
        static JObject GetUserJSON(string CodeTypeID, string CodeID, Session epiSession)
        {
            JObject jObject = new JObject();

            UserCodesImpl userCodesBO = WCFServiceSupport.CreateImpl<Ice.Proxy.BO.UserCodesImpl>(epiSession, Ice.Proxy.BO.UserCodesImpl.UriPath);
            bool b;
            var userCodesDS = userCodesBO.GetRows("CodeTypeID = '" + CodeTypeID + "'", "CodeID = '" + CodeID + "'", 0, 0, out b);
            if (userCodesDS.UDCodes.Count == 1)
            {
                string json = userCodesDS.UDCodes[0]["JSONData_c"].ToString();
                if (!String.IsNullOrEmpty(json))
                {
                    try
                    {
                        jObject = JObject.Parse(json);

                    }
                    catch
                    { }

                }

            }
            return jObject;

        }

    }

    #endregion ATCustomer


}
