﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ice.Core;
using Erp.Proxy.BO;
//using Ice.Proxy.BO;
using Ice.Lib.Framework;
using Erp.BO;
//using Ice.BO;

namespace CustomerTester
{
    public partial class Form1 : Form
    {

        Session epiSession;

        public Form1()
        {
            InitializeComponent();

            string configFile = @"C:\Epicor\ERP10.1ClientTest\Client\config\EpicorTest10.sysconfig";
            //string configFile = @"C:\Epicor\ERP10.1Client\Client\config\EpicorLive10.sysconfig";
            epiSession = new Session("manager", "manager", Session.LicenseType.Default, configFile);
            //            UserCodesImpl userCodesBO = WCFServiceSupport.CreateImpl<Ice.Proxy.BO.UserCodesImpl>(epiSession, Ice.Proxy.BO.UserCodesImpl.UriPath);

        }

        private void button1_Click(object sender, EventArgs e)
        {

            string custID = textBox1.Text;
            CustomerImpl customerBo = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.CustomerImpl>(epiSession, Erp.Proxy.BO.CustomerImpl.UriPath);
            var cust = customerBo.GetByCustID(custID, false);

            ATCustomer.DefaultCustomerCredit(cust.Customer[0], epiSession);

            ATCustomer.DefaultBankAndPaymentFromTerms(cust.Customer[0], epiSession);



        }

        private void button2_Click(object sender, EventArgs e)
        {
            string custID = textBox1.Text;
            CustomerImpl customerBo = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.CustomerImpl>(epiSession, Erp.Proxy.BO.CustomerImpl.UriPath);
            var cust = customerBo.GetByCustID(custID, false);
            customerBo.GetAllShipTo(cust.Customer[0].CustNum, cust);
            textBox2.Text = ATCustomer.GetSNTPPool(epiSession);

            bool aTPrimaryIPReadOnly = false;
            bool aTSecondIPReadOnly = false;
            bool timeServerTypeReadOnly = false;
            var shipTo = cust.ShipTo[0];
            ATCustomer.SetWiFiFields(shipTo, epiSession);
            ATCustomer.SetWiFiReadonly(shipTo, ref aTPrimaryIPReadOnly, ref aTSecondIPReadOnly, ref timeServerTypeReadOnly);

            textBox2.Text = shipTo["ATPrimaryIP"].ToString();
            textBox3.Text = shipTo["ATSecondIP"].ToString();
            textBox4.Text = shipTo["TimeServerType"].ToString();

        }
    }
}
