﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using Epicor.Data;
using Epicor.Hosting;
using Ice.Contracts;
using Ice.ExtendedData;
using Ice.Lib;
using Ice.Tables;
using Ice.Tablesets;

namespace ATS
{
    static class CommonFunctions

    {
        public static string DefaultEmail(string company, int custNum, int orderNum, Erp.ErpContext dataContext)
        {

            //get the order
            var orderHedRow = (from row in dataContext.OrderHed where row.OrderNum == orderNum && row.Company == company select row).FirstOrDefault();

            string email = "";

            if (orderHedRow != null)
            {
                email = orderHedRow["InvoiceEmail_c"].ToString();
            }

            if (System.String.IsNullOrEmpty(email)) //are we still missing the email?
            {
                //get the current customer
                var customerRow = (from row in dataContext.Customer where row.CustNum == custNum && row.Company == company select row).FirstOrDefault();
                if (customerRow != null)
                {
                    email = customerRow["InvoiceEmail_c"].ToString();
                }

            }

            return email;
        }

    }
}
