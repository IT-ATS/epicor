﻿using Ice.BO;
using Ice.Core;
using Ice.Lib.Framework;
using Ice.Proxy.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartenderTester
{


    #region ATBartender

    class ATBartender
    {

        static public void PrintAllJobLabels(string jobNum, Session epiSession)
        {


            using (var dynamicQueryImpl = WCFServiceSupport.CreateImpl<Ice.Proxy.BO.DynamicQueryImpl>(epiSession, Ice.Proxy.BO.DynamicQueryImpl.UriPath))
            {
                QueryExecutionDataSet eDS = new QueryExecutionDataSet();
                DataTable epDT = eDS.Tables["ExecutionParameter"];

                var labelsDs = dynamicQueryImpl.ExecuteByID("ATS-JobLabels", eDS);
                var labelDv = labelsDs.Tables["Results"].DefaultView;

                labelDv.RowFilter = "JobMtl_JobNum = '" + jobNum + "'";

                PrintFromDataView(labelDv, epiSession);


            }
        }

        static public void PrintFromDataView(DataView dataView, Session epiSession)
        {

            foreach (DataRowView label in dataView)
            {
                decimal qty = (decimal)label["JobMtl_RequiredQty"];
                PrintJobLabel(label["JobMtl_JobNum"].ToString(), (int)label["JobMtl_AssemblySeq"], (int)label["JobMtl_MtlSeq"],
                    label["Part_BartenderBAQ_c"].ToString(), label["Part_BartenderTemplate_c"].ToString(),
                    label["Part_BartenderPrinter_c"].ToString(), label["Company_BartenderFileDrop_c"].ToString(),
                    (int)qty, epiSession);


            }

        }



        static void PrintJobLabel(string jobNum, int assemblySeq, int mtlSeq, string baq, string template, string printer, string bartenderFileDrop, int qty, Session epiSession)
        {
            using (var dynamicQueryImpl = WCFServiceSupport.CreateImpl<Ice.Proxy.BO.DynamicQueryImpl>(epiSession, Ice.Proxy.BO.DynamicQueryImpl.UriPath))
            {

                if (string.IsNullOrEmpty(bartenderFileDrop) || string.IsNullOrEmpty(printer))
                    return;
                QueryExecutionDataSet eDS = new QueryExecutionDataSet();
                DataTable epDT = eDS.Tables["ExecutionParameter"];
                epDT.Rows.Add("JobNum", jobNum, "string", "false", null, "A");
                epDT.Rows.Add("AssemblySeq", assemblySeq, "int", "false", null, "A");
                epDT.Rows.Add("MtlSeq", mtlSeq, "int", "false", null, "A");
                epDT.Rows.Add("Qty", qty, "int", "false", null, "A");

                var printDs = dynamicQueryImpl.ExecuteByID(baq, eDS);



                StringBuilder sb = new StringBuilder();
                sb.AppendLine(@"%BTW% /AF=""" + template + @""" /D=""<Trigger File Name>"" /PRN="""
                + printer + @""" /DBTEXTHEADER=3 /R=3 /P ");
                sb.AppendLine("%END%");

                IEnumerable<string> columnNames = printDs.Tables["Results"].Columns.Cast<DataColumn>().
                                                  Select(column => column.ColumnName);
                sb.AppendLine(string.Join(",", columnNames));

                foreach (DataRow row in printDs.Tables["Results"].Rows)
                {
                    IEnumerable<string> fields = row.ItemArray.Select(field =>
                      string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                    sb.AppendLine(string.Join(",", fields));
                }

                if (bartenderFileDrop.Substring(bartenderFileDrop.Length - 1) != @"\")
                    bartenderFileDrop += @"\";
                String timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                File.WriteAllText(bartenderFileDrop + jobNum + timeStamp + ".bt", sb.ToString());



            }
        }
    }

    #endregion ATBartender


}
