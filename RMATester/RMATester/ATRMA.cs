﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ice.Core;
using Erp.Proxy.BO;
//using Ice.Proxy.BO;
using Ice.Lib.Framework;
using Erp.BO;
using Erp.Adapters;
using System.Data;
using System.Net.Mail;
using Erp.Proxy.Rpt;
using Erp.Rpt;
//using Ice.BO;

namespace RMATester
{

    #region ATRMA


    class ATRMA
    {

        static public int CreateSalesOrder(int rmaNum, Ice.Core.Session epiSession )
        {
            string uom = "";
            string warning = "";
            string prodCode = "";
            string mktCmpg = "";
            //bool allWarrantyParts = true;


            //Determine 4 business and 10 business days from today
            DateTime fourDays;
            DateTime tenDays;

            fourDays = FindBusinessDay(5, DateTime.Today); //One extra day needs to be added to correctly calculate the dates
            tenDays = FindBusinessDay(11, DateTime.Today);

            //Get Marketing Campaign from Company Config

            Erp.Proxy.BO.CompanyImpl companyBo = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.CompanyImpl>(epiSession, Erp.Proxy.BO.CompanyImpl.UriPath);

            Erp.BO.CompanyDataSet companyDS = companyBo.GetByID(epiSession.CompanyID);

            mktCmpg = companyDS.Tables["CRSyst"].Rows[0]["DefMktgCampaignID"].ToString();
            companyBo.Dispose();


            RMAProcImpl rmaProc = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.RMAProcImpl>(epiSession, Erp.Proxy.BO.RMAProcImpl.UriPath);
            //		Epicor.Mfg.BO.RMAProc rmaProc = new Epicor.Mfg.BO.RMAProc(((Epicor.Mfg.Core.Session)oTrans.EpiBaseForm.Session).ConnectionPool);
            RMAProcDataSet ds_rmaProc = rmaProc.GetByID(rmaNum);
            var rMAHeadRow = ds_rmaProc.RMAHead[0];


            /*
                        var oTrans = new ILauncher(epiSession);
                        HelpDeskAdapter adpHD = new HelpDeskAdapter(oTrans);
                        adpHD.BOConnect();*/
            int caseNum = Convert.ToInt32(rMAHeadRow["HDCaseNum"].ToString());

//            adpHD.GetByID(caseNum);

            //Create OrderHed information
            int newOrderNum;


            //create OrderLine information


            SalesOrderImpl so = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.SalesOrderImpl>(epiSession, Erp.Proxy.BO.SalesOrderImpl.UriPath);

            SalesOrderDataSet salesOrderDataSet = new SalesOrderDataSet();

            CustomerImpl customerImpl = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.CustomerImpl>(epiSession, Erp.Proxy.BO.CustomerImpl.UriPath);


            CustomerDataSet customerDataSet = customerImpl.GetByID((int)rMAHeadRow["RMACustNum_c"]);


            so.GetNewOrderHed(salesOrderDataSet);
            var orderHed = salesOrderDataSet.OrderHed[0];
            orderHed.CustNum = (int)rMAHeadRow["RMACustNum_c"];
            orderHed.BTCustNum = (int)rMAHeadRow["RMACustNum_c"];
            orderHed.CustomerCustID = customerDataSet.Customer[0].CustID;

            so.ChangeSoldToID(salesOrderDataSet);

            orderHed.ShipToNum = (string)rMAHeadRow["RMAShipToNum_c"];
            so.ChangeShipToID(salesOrderDataSet);


            orderHed.PONum = "RMA" + rMAHeadRow["RMANum"].ToString();
            orderHed.HDCaseNum = (int)rMAHeadRow["HDCaseNum"];
            orderHed.ShpConNum = (int)rMAHeadRow["RMAShipToConNum_c"];
            orderHed.PrcConNum = (int)rMAHeadRow["RMASoldToConNum_c"];
            if (rMAHeadRow.UseOTS)
            {
                orderHed.UseOTS = true;
                orderHed.OTSAddress1 = rMAHeadRow.OTSAddress1;
                orderHed.OTSAddress2 = rMAHeadRow.OTSAddress2;
                orderHed.OTSAddress3 = rMAHeadRow.OTSAddress3;
                orderHed.OTSCity = rMAHeadRow.OTSCity;
                orderHed.OTSName = rMAHeadRow.OTSName;
                orderHed.OTSPhoneNum = rMAHeadRow.OTSPhoneNum;
                orderHed.OTSState = rMAHeadRow.OTSState;
                orderHed.OTSZIP = rMAHeadRow.OTSZIP;
                orderHed.OTSCountryNum = rMAHeadRow.OTSCountryNum;

                so.ChangeHedUseOTS(salesOrderDataSet);

            }

            so.Update(salesOrderDataSet);
            newOrderNum = orderHed.OrderNum;


            //find if all parts or Warranty, NonWarranty or mixed
            int WarPartsCount = 0;
            int NonWarPartsCount = 0;


            foreach (DataRow dr in ds_rmaProc.Tables["RMADtl"].Rows)
            {



                so.GetNewOrderDtl(salesOrderDataSet, newOrderNum);
                var newRow = salesOrderDataSet.OrderDtl[salesOrderDataSet.Tables["OrderDtl"].Rows.Count - 1];
                newRow.PartNum = dr["PartNum"].ToString();

                bool b;
                string whereClause = "PartNum = '" + dr["PartNum"].ToString() + "'";

                PartImpl partImpl = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.PartImpl>(epiSession, Erp.Proxy.BO.PartImpl.UriPath);
                PartListDataSet partListDataSet = partImpl.GetList(whereClause, 0, 0, out b);
                if (partListDataSet.PartList.Rows.Count == 1)
                {
                    prodCode = partListDataSet.PartList[0].ProdCode;
                    uom = partListDataSet.PartList[0].IUM;
                    prodCode = partListDataSet.PartList[0].ProdCode;

                }

                if (prodCode == "260300")//"Warranty"
                {
                    WarPartsCount++;
                }
                else
                {
                    NonWarPartsCount++;
                }




                so.ChangePartNum(salesOrderDataSet, false, uom);

        //        newRow.RevisionNum = dr["RevisionNum"].ToString();
                newRow.SellingQuantity = (decimal)dr["ReturnQty"];
                newRow.OrderComment = dr["Note"].ToString();
                newRow.MktgEvntSeq = 16;
                newRow.MktgCampaignID = mktCmpg;
                newRow.LineDesc = dr["LineDesc"].ToString();
                newRow.ProjectID = rMAHeadRow["ShortChar01"].ToString();


                so.ChangeSellingQuantity(salesOrderDataSet, false, (decimal)dr["ReturnQty"], out warning);

                so.Update(salesOrderDataSet);

            }
            foreach (SalesOrderDataSet.OrderDtlRow soRow in salesOrderDataSet.OrderDtl.Rows)
            {
                if (WarPartsCount > 0 && NonWarPartsCount == 0) //Only warranty parts
                {
                    soRow.NeedByDate = fourDays;//DateTime.Now.AddDays(4);
                    soRow.RequestDate = fourDays;//DateTime.Now.AddDays(4);
                }
                else //Mixed or NonWarranty parts
                {
                    soRow.NeedByDate = tenDays; //DateTime.Now.AddDays(10);
                    soRow.RequestDate = tenDays; //DateTime.Now.AddDays(10);

                }

            }

            //Update Some fields in the SalesOrder Header
            if (salesOrderDataSet.OrderHed.Rows.Count > 0)
            {
                
                string caseOwner = "";
                caseOwner = GetCaseRep(caseNum, epiSession);

                orderHed["ShortChar01"] = "R";
                if (caseOwner == "")
                    orderHed["SalesRepCode1"] = "PRODSVC";
                else
                    orderHed["SalesRepCode1"] = caseOwner;

                if (WarPartsCount > 0 && NonWarPartsCount == 0)//Only warranty parts
                {
                    orderHed["NeedByDate"] = fourDays;//DateTime.Now.AddDays(4);
                    orderHed["RequestDate"] = fourDays;//DateTime.Now.AddDays(4);
                }
                else //Mixed or NonWarranty parts
                {
                    orderHed["NeedByDate"] = tenDays; //DateTime.Now.AddDays(10);
                    orderHed["RequestDate"] = tenDays; //DateTime.Now.AddDays(10);
                }
                if (GetDefaultCreditCardOrder(orderHed.CustNum, epiSession))
                    MessageBox.Show("This customer is a credit card customer.");
                so.Update(salesOrderDataSet);

            }

            return newOrderNum;

        }
        public static DateTime FindBusinessDay(int numberOfBusinessDays, DateTime fromDate)
        {
            //This is used to count the number of business days
            int businessDays = 0;
            int noOfDays = numberOfBusinessDays;
            for (int i = 1; i <= numberOfBusinessDays; i++)
            {
                //if current date is the WeekEnd increase the
                //numberOfBusinessDays with one
                //this is because we need one more loop ocurrrence
                if (IsWeekEnd(fromDate))
                    numberOfBusinessDays++;
                else
                    businessDays++;

                //When businessDays is not equal to noOfDays,
                //add one day in the current date.
                if (businessDays != noOfDays)
                {
                    fromDate = fromDate.AddDays(1);
                }
                else
                {
                    break;
                }
            }
            return fromDate;
        }
        /// <summary>
        /// Gets the owner case rep from the case
        /// </summary>
        /// <param name="CaesNum"></param>
        /// <param name="epiSession"></param>
        /// <returns>the case owner or "" if none is found</returns>
        static private string GetCaseRep(int CaesNum, Session epiSession)
        {

            string salesRep = "";
            if (CaesNum > 0)
            {
                try
                {
                    HelpDeskImpl caseBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.HelpDeskImpl>(epiSession, Erp.Proxy.BO.HelpDeskImpl.UriPath);
                    var caseDS = caseBO.GetByID(CaesNum);
                    salesRep = caseDS.HDCase[0].CaseOwner;
                    caseBO.Dispose();
                }
                catch
                { }


            }


            return salesRep;
        }

        static bool GetDefaultCreditCardOrder(int custNum, Session epiSession)
        {
            bool creditCardOrder = false;
            if (custNum > 0)
            {
                try
                {
                    CustomerImpl customerBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.CustomerImpl>(epiSession, Erp.Proxy.BO.CustomerImpl.UriPath);
                    var custDS = customerBO.GetByID(custNum);

                    creditCardOrder = custDS.Customer[0].CreditCardOrder;

                    customerBO.Dispose();
                }
                catch { }
            }
            return creditCardOrder;
        }


        /// <summary>
        /// takes an order and creats a job for it printing the traveler when it's done.
        /// The sales order will also be updated to be make direct.
        /// History:
        /// 7/17/17 converted to bo and added the order update
        /// </summary>
        /// <param name="ordernum"></param>
        /// <param name="epiSession"></param>
        public static void CreateJobs(int ordernum, Session epiSession)
        {

            SalesOrderImpl salesOrderBO = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.SalesOrderImpl>(epiSession, Erp.Proxy.BO.SalesOrderImpl.UriPath);

            var salesOrderDs = salesOrderBO.GetByID(ordernum);
            foreach (SalesOrderDataSet.OrderRelRow orr in salesOrderDs.OrderRel.Rows)
            {
                if (orr.Make == false)
                {
                    orr.Make = true;
                    orr.RowMod = "U";
                }

            }
            salesOrderBO.Update(salesOrderDs);


            OrderJobWizImpl orderJobWizBo = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.OrderJobWizImpl>(epiSession, Erp.Proxy.BO.OrderJobWizImpl.UriPath);

            int jobOrder = ordernum;


            OrderJobWizDataSet orderJobWizDs = orderJobWizBo.GetMatrix(jobOrder, 0, 0);

            foreach (OrderJobWizDataSet.JWJobOrderDtlRow dr in orderJobWizDs.JWJobOrderDtl.Rows)
            {
                dr.JobChkBox = true;
                dr.DetailChkBox = true;

            }

            string returned = "";
            orderJobWizBo.ValidateJobs(orderJobWizDs, out returned);
            orderJobWizDs.JWJobOrderDtl.Rows[0]["RowMod"] = "U";


            orderJobWizBo.CreateJobs(orderJobWizDs, out returned);

            if (returned != "")
            {
                MessageBox.Show("Error creating job: " + returned.ToString());
            }
            else
            {
                foreach (OrderJobWizDataSet.JWOrderRelRow dr in orderJobWizDs.JWOrderRel.Rows)
                {

                    printTraveler(dr.JobNum, epiSession);

                }

            }

            orderJobWizBo.Dispose();
            salesOrderBO.Dispose();
        }


        public static bool IsWeekEnd(DateTime dateTime)
        {
            bool isWeekEnd = false;
            switch (dateTime.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                case DayOfWeek.Saturday:
                    isWeekEnd = true;
                    break;
            }
            return isWeekEnd;
        }


        public static void SendEmail(System.Net.Mail.MailMessage msg,  Session epiSession)
        {
            string password = "";

            using (Erp.Proxy.BO.CompanyImpl companyImpl = WCFServiceSupport.CreateImpl<Erp.Proxy.BO.CompanyImpl>(epiSession, Erp.Proxy.BO.CompanyImpl.UriPath))
            {
                var companyDS = companyImpl.GetByID(epiSession.CompanyID);


                password = (string)companyDS.Company[0]["EMailPassword_c"];
                if ((bool)companyDS.Company[0]["LiveDB_c"] == false)
                {
                    msg.Subject = msg.Subject + " Test: Original Recipient: " + msg.To.ToString();
                    msg.To.Clear();
                    msg.To.Add(new MailAddress(@"it@atsclock.com"));
                }

            }
            try
            {

                using (Ice.Proxy.BO.CompanyImpl companyImpl = WCFServiceSupport.CreateImpl<Ice.Proxy.BO.CompanyImpl>(epiSession, Ice.Proxy.BO.CompanyImpl.UriPath))
                {
                    var companyDS = companyImpl.GetByID(epiSession.CompanyID);
                    SmtpClient client = new SmtpClient();
                    client.Host = companyDS.Company[0].SMTPServer;

                    client.Credentials = new System.Net.NetworkCredential(companyDS.Company[0].EmailFromAddr, password);
                    client.Port = companyDS.Company[0].SMTPPort;
                    client.EnableSsl = companyDS.Company[0].SMTPEnableSSL;
                    client.Send(msg);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Mail exception: " + ex.ToString());
            }
        }



        static private void printTraveler(string jobnum, Ice.Core.Session session)
        {
            //obtain a reference to the current session
            int terminalID = session.GetTerminalID();

            // Obtain WorkStationID
            string workstationID = System.Environment.MachineName;



            System.Drawing.Printing.PrinterSettings settings = new System.Drawing.Printing.PrinterSettings();
            string defaultPrinter = settings.PrinterName;


            System.Drawing.Printing.PageSettings ps = (System.Drawing.Printing.PageSettings)settings.DefaultPageSettings.Clone();

            ps.Margins.Left = 16;
            ps.Margins.Right = 16;
            ps.Margins.Top = 25;
            ps.Margins.Bottom = 100;
            string pageSettings = ps.ToString();
            pageSettings = pageSettings.Substring(15);
            pageSettings = pageSettings.Substring(0, pageSettings.Length - 1);
            pageSettings = pageSettings.Replace("Landscape=False", "Landscape=True");


            JobTravImpl TravRrpt = WCFServiceSupport.CreateImpl<JobTravImpl>(session, JobTravImpl.UriPath);

            JobTravDataSet TravRrptDS = new JobTravDataSet();

            TravRrptDS = TravRrpt.GetNewParameters();
            TravRrptDS.Tables["JobTravParam"].Rows[0]["Jobs"] = jobnum;
            TravRrptDS.Tables["JobTravParam"].Rows[0]["AgentID"] = "SystemTaskAgent";
            TravRrptDS.Tables["JobTravParam"].Rows[0]["AgentTaskNum"] = 0;
            TravRrptDS.Tables["JobTravParam"].Rows[0]["AutoAction"] = "PRINT";
            TravRrptDS.Tables["JobTravParam"].Rows[0]["ShpSchd"] = true;
            TravRrptDS.Tables["JobTravParam"].Rows[0]["ReportStyleNum"] = "1002";
            TravRrptDS.Tables["JobTravParam"].Rows[0]["WorkstationID"] = workstationID + " " + terminalID.ToString();

            TravRrptDS.Tables["JobTravParam"].Rows[0]["PrinterName"] = defaultPrinter;
            TravRrptDS.Tables["JobTravParam"].Rows[0]["RptPageSettings"] = pageSettings;
//            pageSettings = "Color = False, Landscape = True, Margins =[Margins Left = 16 Right = 16 Top = 25 Bottom = 100], PaperSize =[PaperSize Letter Kind = Letter Height = 1100 Width = 850], PaperSource =[PaperSource Automatically Select Kind = FormSource], PrinterResolution =[PrinterResolution X = 600 Y = 600]";
            TravRrptDS.Tables["JobTravParam"].Rows[0]["RptPrinterSettings"] = @"PrinterName=""" + defaultPrinter + @""",Copies=1,Collate=False,Duplex=Simplex,FromPage=0,ToPage=0";



            TravRrpt.SubmitToAgent(TravRrptDS, "SystemTaskAgent", 0, 0, "Epicor.Mfg.UIRpt.JobTravJobEntry");

        }




    }




    #endregion ATRMA



}
