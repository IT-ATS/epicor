﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ice.Core;
//using Erp.Proxy.BO;
//using Ice.Proxy.BO;
using Ice.Lib.Framework;
using Erp.BO;
using System.Net.Mail;
//using Ice.BO;

namespace RMATester
{
    public partial class Form1 : Form
    {

        Session epiSession;

        public Form1()
        {
            InitializeComponent();

            string configFile = @"C:\Epicor\ERP10.1ClientTest\Client\config\EpicorTest10.sysconfig";
            //string configFile = @"C:\Epicor\ERP10.1Client\Client\config\EpicorLive10.sysconfig";
            epiSession = new Session("manager", "manager", Session.LicenseType.Default, configFile);
            //            UserCodesImpl userCodesBO = WCFServiceSupport.CreateImpl<Ice.Proxy.BO.UserCodesImpl>(epiSession, Ice.Proxy.BO.UserCodesImpl.UriPath);

        }

        private void button1_Click(object sender, EventArgs e)
        {

            int soNum = ATRMA.CreateSalesOrder(Convert.ToInt32(rmaTb.Text), epiSession);
            ATRMA.CreateJobs(soNum, epiSession);


        }

        private void button2_Click(object sender, EventArgs e)
        {

            using (System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage())
            {

                msg.To.Add(new MailAddress("carsonr@atsclock.com"));
                msg.From = new MailAddress("atsemails@atsclock.com");
                msg.Subject = "Your Product Has Been Received ";
                msg.Body = "Hello "  + ",<br><br>" +
                            "We are notifying you to let you know we have received your product return RMA# " +
                            " back at our facility. Our repair team will begin working on it, and we’ll ship it back to you ASAP.<br><br>" +
                            "If you have any questions, please call us at 800-328-8996 or email " + "<br><br><br>" +
                            "All the best,<br>" +
                            "Your Support Team at American Time";
                msg.IsBodyHtml = true;

                ATRMA.SendEmail(msg, epiSession);

            }




        }
    }
}
