﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ice.Core;
using Erp.Proxy.BO;
//using Ice.Proxy.BO;
using Ice.Lib.Framework;
using Erp.BO;
using System.Windows.Forms;


namespace CaseTester
{

    #region ATCase


    class ATCase
    {
        /// <summary>
        /// Creates an RMA and line based on the given case
        /// </summary>
        /// <param name="caseNum">The case number</param>
        /// <param name="epiSession">the current Epicor Session</param>
        /// <returns>The created RMA number or 0 if no RMA was created.</returns>
        public static int CreateRMA(int caseNum, Session epiSession)
        {
            int rmaNum = 0;
            if (caseNum == 0)
                return rmaNum;

            using (HelpDeskImpl helpDeskBO = WCFServiceSupport.CreateImpl<HelpDeskImpl>(epiSession, HelpDeskImpl.UriPath))
            {

                var helpDeskDS = helpDeskBO.GetByID(caseNum);

                //make sure we have exactly 1 case
                if (helpDeskDS.HDCase == null)
                    return rmaNum;
                if (helpDeskDS.HDCase.Count > 1)
                    return rmaNum;

                if (String.IsNullOrEmpty(helpDeskDS.HDCase[0].PartNum))
                {
                    MessageBox.Show("Please Enter a Part Number.");
                    return rmaNum;
                }

                var caseRow = helpDeskDS.HDCase[0];
                using (PartImpl partImpl = WCFServiceSupport.CreateImpl<PartImpl>(epiSession, PartImpl.UriPath))
                {
                    var partDS = partImpl.GetByID(caseRow.PartNum);
                    if (partDS.Part[0].ProdCode.Substring(0, 1) == "3")
                    {
                        // we need a project on parts with product groups that start with "3"
                        if (String.IsNullOrEmpty(caseRow.ProjectID))
                        {
                            MessageBox.Show("Please Enter a Project.");
                            return rmaNum;

                        }
                    }

                }

                using (RMAProcImpl rmaBO = WCFServiceSupport.CreateImpl<RMAProcImpl>(epiSession, RMAProcImpl.UriPath))
                {
                    RMAProcDataSet rmaDS = new RMAProcDataSet();

                    rmaBO.GetNewRMAHead(rmaDS);

                    var rmaHeadRow = rmaDS.RMAHead[0];
                    rmaHeadRow.CustNum = caseRow.CustNum;
                    rmaHeadRow.BTCustNum = caseRow.CustNum;
                    rmaHeadRow.HDCaseNum = caseRow.HDCaseNum;
                    rmaHeadRow.InvoiceNum = caseRow.InvoiceNum;

                    string custID = (string)caseRow["ShortChar01"];
                    int custNum = 0;
                    using (CustomerImpl custBO = WCFServiceSupport.CreateImpl<CustomerImpl>(epiSession, CustomerImpl.UriPath))
                    {
                        bool b;
                        var custListDS = custBO.GetList("CustID = '" + custID + "'", 1, 1, out b);
                        custNum = custListDS.CustomerList[0].CustNum;
                    }

                    if (custNum == 0)
                    {
                        MessageBox.Show("Please Enter a Customer.");
                        return rmaNum;
                    }

                    rmaHeadRow["RMACustNum_c"] = custNum;
                    string shipToNum = (string)caseRow["ShortChar02"];
                    rmaHeadRow["RMAShipToNum_c"] = shipToNum;

                    int perConID = 0;
                    int.TryParse(caseRow["Number01"].ToString(), out perConID);
                    rmaHeadRow["RMAPerConID_c"] = perConID;


                    using (CustCntImpl custCntBO = WCFServiceSupport.CreateImpl<CustCntImpl>(epiSession, CustCntImpl.UriPath))
                    {
                        bool b;
                        string whereClause = "CustNum = '" + custNum.ToString() + "' and PerConID = '" + perConID.ToString() + "'";
                        var custCntListDS = custCntBO.GetList(whereClause, 1, 1, out b);
                        if (custCntListDS.CustCntList.Count == 1)
                            rmaHeadRow["RMAShipToConNum_c"] = custCntListDS.CustCntList[0].ConNum;
                    }



                    rmaBO.Update(rmaDS);
                    rmaNum = rmaHeadRow.RMANum;

                    rmaBO.GetNewRMADtl(rmaDS, rmaNum);
                    var rmaDtlRow = rmaDS.RMADtl[0];

                    rmaDtlRow.PartNum = caseRow.PartNum;
                    rmaDtlRow.LineDesc = caseRow.PartDescription;
                    rmaDtlRow.ReturnQty = caseRow.Quantity;
                    rmaDtlRow.ReturnQtyUOM = caseRow.QuantityUOM;
                    rmaDtlRow.OrderNum = caseRow.OrderNum;
                    rmaDtlRow.OrderLine = caseRow.OrderLine;
                    rmaDtlRow.OrderRelNum = caseRow.OrderRelNum;
                    rmaDtlRow.InvoiceNum = caseRow.InvoiceNum;
                    rmaDtlRow.InvoiceLine = caseRow.InvoiceLine;

                    rmaDtlRow.ReturnReasonCode = "RMA";
                    rmaBO.Update(rmaDS);


                }



                return rmaNum;
            }
        }

    }


#endregion ATCase

}
