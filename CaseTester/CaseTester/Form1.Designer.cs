﻿namespace CaseTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.caseTb = new System.Windows.Forms.TextBox();
            this.createRMABtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // caseTb
            // 
            this.caseTb.Location = new System.Drawing.Point(145, 61);
            this.caseTb.Name = "caseTb";
            this.caseTb.Size = new System.Drawing.Size(106, 20);
            this.caseTb.TabIndex = 0;
            this.caseTb.Text = "7529";
            // 
            // createRMABtn
            // 
            this.createRMABtn.Location = new System.Drawing.Point(331, 61);
            this.createRMABtn.Name = "createRMABtn";
            this.createRMABtn.Size = new System.Drawing.Size(101, 33);
            this.createRMABtn.TabIndex = 1;
            this.createRMABtn.Text = "create rma";
            this.createRMABtn.UseVisualStyleBackColor = true;
            this.createRMABtn.Click += new System.EventHandler(this.createRMABtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 522);
            this.Controls.Add(this.createRMABtn);
            this.Controls.Add(this.caseTb);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox caseTb;
        private System.Windows.Forms.Button createRMABtn;
    }
}

