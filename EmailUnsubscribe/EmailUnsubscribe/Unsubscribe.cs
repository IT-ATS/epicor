﻿#define Debug
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ice.Core;
using Erp.Proxy.BO;
//using Ice.Proxy.BO;
using Ice.Lib.Framework;
using Erp.BO;
//using Ice.BO;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.IO;

namespace EmailUnsubscribe
{

    #region Unsubscribe


    /// <summary>
    /// Takes an export from hubspot and marks all the person contacts for the email addresses as no marketing email.
    /// History:
    /// 12/7/2017: update to updated status on completion.  
    /// Initial release 12/4/17
    /// </summary>
    class Unsubscribe : IDisposable
    {
        Session epiSession;
        BackgroundWorker worker;
        string status = "";


#if Debug
        TextBox statusTb;
        Button browseBtn;
        public Unsubscribe(Session _epiSession, Button _browseBtn, TextBox _statusTb )
#else
        EpiTextBox statusTb;
        EpiButton browseBtn;
        public Unsubscribe(Session _epiSession, EpiButton _browseBtn, EpiTextBox _statusTb )

#endif
        {
            epiSession = _epiSession;
            browseBtn = _browseBtn;
            statusTb = _statusTb;

            browseBtn.Click += new System.EventHandler(browseBtn_Click);

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);


        }
        public void Dispose()
        {
            browseBtn.Click -= new System.EventHandler(browseBtn_Click);
            worker.ProgressChanged -= new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.DoWork -= new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker = null;

        }


        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                statusTb.Text = e.UserState.ToString();
            }

        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                statusTb.Text = e.Error.Message + Environment.NewLine + status;

            }
            else if (e.Cancelled)
            {
                statusTb.Text = "Cancelled" + Environment.NewLine + status;
            }
            else
            {
                statusTb.Text = "Complete" + Environment.NewLine + status;
            }

            //reset
            browseBtn.Enabled = true;

        }
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string csvFile = (string)e.Argument;
            var unsubscribeDt = ConvertCSVtoDataTable(csvFile);

            DateTime time = DateTime.Now;
            using (PerConImpl perConBO = WCFServiceSupport.CreateImpl<PerConImpl>(epiSession, PerConImpl.UriPath))
            {
                foreach (DataRow emailRow in unsubscribeDt.Rows)
                {
                    if (DateTime.Now > time.AddSeconds(.5))  //only update progess every .5 seconds
                    {
                        worker.ReportProgress(0, status);
                        time = DateTime.Now;

                    }

                    string emailAddress = (String)emailRow[0];  //the first column contains a the email address to remove
                    bool b;
                    var perconListDS = perConBO.GetList("EmailAddress = '" + emailAddress + "'", 0, 0, out b);
                    if (perconListDS.PerConList.Rows.Count == 0)
                    {
                        string message = "Email Address:" + emailAddress + " not found in Epicor";
                        status = message + System.Environment.NewLine + status;

                    }
                    foreach (PerConListDataSet.PerConListRow perconRow in perconListDS.PerConList.Rows)
                    {

                        var perConDS = perConBO.GetByID(perconRow.PerConID);
                        if (!(bool)perConDS.PerCon[0]["CheckBox01"])
                        {
                            perConDS.PerCon[0]["CheckBox01"] = true;
                            perConDS.PerCon[0].RowMod = "U";
                            perConBO.Update(perConDS);

                            string message = "Person/Contact:" + perconRow.PerConID.ToString() + " Email Address:" + emailAddress + " hase been marked as no marketing email";
                            status = message + System.Environment.NewLine + status;


                        }
                        else
                        {
                            string message = "No update needed on Person/Contact:" + perconRow.PerConID.ToString() + " Email Address:" + emailAddress ;
                            status = message + System.Environment.NewLine + status;

                        }

                    }


                }

            }




        }

        private void browseBtn_Click(object sender, EventArgs e)
        {
            browseBtn.Enabled = false;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            openFileDialog.Filter = "csv files(*.csv) | *.csv";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                worker.RunWorkerAsync(openFileDialog.FileName);

            }
            else
            {
                browseBtn.Enabled = true;

            }
        }


        DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                string escapedLine = Escape(line);
                string[] rows = escapedLine.Split(',');
                if (rows.Length == headers.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = Unescape(rows[i]);
                    }
                    dt.Rows.Add(dr);

                }
            }
            return dt;
        }


        string Escape(string s)
        {
            bool foundQuote = false;
            char[] c = s.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == '"') //we found a quote
                {
                    if (s.Substring(i + 1).Contains("\"") || foundQuote)   // is there another " or did we already find one
                        foundQuote = !foundQuote;

                }
                if (c[i] == ',' && foundQuote)
                {
                    c[i] = '~';

                }


            }
            s = new string(c);
            return (s);
        }


        string Unescape(string s)
        {
            s = s.Replace("~", ",");
            s = s.Replace("\"", "");
            return s;
        }


    }

    #endregion Unsubscribe

}
